%
% fluid simulation
% (c) 2013
%

\chapter{Analytical description of fluid mechanics}

In this chapter an analytic description of fluid dynamics is derived. Our aim is to establish an algorithm, which can be run in realtime, hence keeping our fluid model as simple as possible is of great importance.

As we will see, the compressibility of a fluid plays a key roll in its behaviour. Water is a fluid, which requires a lot of force to be compressed. Although air isn't incompressible, its resistance against compression causes effects like vortices, which can also be observed in incompressible fluids. Hence assuming that our fluid is incompressible is a good choice. 

Furthermore the behaviour of fluids is governed by its viscosity. While fluids with close to zero viscosity exhibit vortices even if small forces are applied, in fluids with high viscosity, flows are mostly laminar. The viscosity of an air or water volume almost doesn't change, if the force applied to it changes. It's practical to assume that viscosity in such fluids is constant. Fluids with constant viscosity are called Newtonian fluids.

\section{Modelling fluid as a continuum}

At microscopic scales we could see the individual fluid molecules, count them, and observe that empty space separates the individual particles from each other. But using a model for our purposes with such properties would be inpractical. As long as we remain at macroscopic scales, we can divide the fluid volume into smaller fluid volumes without observing individual particles. Hence the descripion of fluid as a continuum

\begin{equation}
\label{eq:nse_continuum}
\Omega \subseteq \mathbb{R}^3
\end{equation}

of particles such that the particles are tightly packed in the volume, and any point of the volume represents a fluid particle is appropriate. Such particles are also called continuum bodies in the context of continuum mechanics. The continuum itself is a time-invariant set composed of a countable number of connected partitions with a differentiable surface $\partial \Omega$. Each $n_P$-dimensional physical property $P$, for instance velocity, pressure or density, can be described as a time-dependend field

\begin{equation}
\label{eq:nse_field}
P: \Omega \times \mathbb{R} \rightarrow D_P \subseteq \mathbb{R}^{n_P} : \left( x , t \right) \mapsto P(x,t)
\end{equation}

, where $P(x,t)$ is the propery of the particle at position $x$ and time $t$. The domain of properties is the set of all possible values the propery of a particle could have. For instance velocity (ignoring the speed of light as a boundary) could be any vector in $\mathbb{R}^3$. An intuitive way of describing the state of the continuum at each point of time would be to trace each particle, and the properties it carries with it. This way of viewing continuum deformation is called Lagrangian description of continuum deformation.

\subsection{Lagrangian description of continuum deformation}

Describing continuum deformation in the Lagrangian way means to describe physical properties in the reference frame of the particles. Let

\begin{equation}
\label{eq:nse_path}
\Phi : \Omega \times \mathbb{R} \rightarrow \Omega : \left(X , t \right) \mapsto \Phi(X,t) = x
\end{equation}

be the trajectory of a particle intersecting position $X$ at timepoint $0$, and

\begin{equation}
\label{eq:nse_lagrangian}
\overline{P}(X,t) := P(\Phi(X,t),t) = P(x,t)
\end{equation}

, then $\overline{P}$ describes the different states of a property from the view of the particles. If a certain property $\overline{P}$ is differentiable, the rate of change

\begin{equation}
\label{eq:nse_materialdev}
\frac{d}{d t} \overline{P} (X,t)
\end{equation}

is called material derivative. The Lagrangian description of continuum deformation makes deriving the influence of the forces on the continuum straight forward, as rates of change can be viewed from the standpoint of the particles: For a body with volume $\Delta V$, density $\rho$, speed $\overline{u}$, and a family of forces $F_i$ Newton's second law of motion states

\begin{equation}
\label{eq:nse_newton}
\overline{m} \cdot \frac{d}{d t} \overline{u} = \Delta V \cdot \rho \cdot \frac{d}{d t} \overline{u} = \sum_{i} F_i
\end{equation}

By letting $\Delta V$ converge to zero, we obtain the material derivative of $\overline{u}$:

\begin{equation}
\label{eq:nse_materialu}
\frac{d}{d t} \overline{u} = \lim_{\Delta V \rightarrow 0} \frac{\sum_{i} F_i}{\rho \cdot \Delta V}
\end{equation}
\\
\\
\begin{figure}[h]
\begin{center}
\includegraphics[height=5cm]{images/newton.png}
\end{center}
\caption{Deformation of a continuum body}
\label{fig:nse_newton_pic}
\end{figure}

The assumption that the sum of the forces is a continuous function implies that the trajectory $\Phi(X,.)$ of a continuum body is differentiable in time with

\begin{equation}
\label{eq:nse_speeddev}
u(x,t) = u(\Phi(X,t),) = \overline{u}(X,t) = \frac{\partial}{\partial t} \Phi(X,t)
\end{equation}

(, and in consequence continuous and connected). As each position of $\Omega$ describes exactly one continuum body, particle trajectories can't split or merged, requiring $\Phi(.,t)$ to be bijective, and hence invertible.

\begin{figure}[h]
\begin{center}
\includegraphics[height=6cm]{images/path.png}
\end{center}
\caption{The trajectory of a continuum body}
\label{fig:nse_path_pic}
\end{figure}

If we only know the velocity field $u$ for each point of time, $\Phi(X,t)$ can be computed by integrating $u$ in time from $0$ to $t$. Hence the differential equations ~\ref{eq:nse_materialu} describe, how the continuum bodies are deformed, if equivalent equations without the trajectory terms can be formulated, and the forcefield $\sum_i F_i$ is given. Describing the continuum this way is called Eulerian description of continuum deformation.

\subsection{Eulerian description of continuum deformation}

Describing continuum deformation in the Eulerian way means to describe physical properties in the reference frame of the continuum itself, and not necessarily in the reference frame of the continuum bodies. In equation ~\ref{eq:nse_lagrangian} the right part of the equation is given in Eulerian coordinates. For each point of time $t$ in Eulerian coordinates $P(x,t)$ retrieves a physical property of the continuum body located at position $x$ at time $t$. An easy to imagine example would be the velocity field $u$ of continuum $\Omega$. A certain point $x \in \Omega$ could be imagined as a weather station measuring the wind. Its evaluations $u(x,t)$ are given in Eulerian coordinates, and measuring the velocity of a certain continuum body currently crossing the weather station with that speed. In contrast a function $\overline{P}(X,t)$, which retrieves the velocity of a dust particle at time $t$, which was located at position $X$ at time $0$, is a good example for a function given in Lagrangian coordinates.

\begin{figure}[h]
\begin{center}
\includegraphics[height=7cm]{images/eulerian.png}
\end{center}
\caption{Eulerian description of a velocity field}
\label{fig:nse_path_pic}
\end{figure}

The material derivative of a property $P$ can be expressed Eulerian coordinates by considering equations ~\ref{eq:nse_path} and ~\ref{eq:nse_speeddev}, and deriving $\overline{P}(X,t)$ the following way:

\begin{gather}
\label{eq:nse_eulerderiv}
\frac{d}{d t} \overline{P}(X,t) = \frac{d}{d t} P(\Phi(X,t),t) \nonumber \\
= \left( \begin{array}{cccc} \frac{\partial}{\partial x} P^{x_1}(Y) & \frac{\partial}{\partial y} P^{x_1}(Y) & \frac{\partial}{\partial z} P^{x_1}(Y) & \frac{\partial}{\partial t} P^{x_1}(Y) \\ \vdots & \vdots & \vdots & \vdots \\ \frac{\partial}{\partial x} P^{x_{n_P}}(Y) & \frac{\partial}{\partial y} P^{x_{n_P}}(Y) & \frac{\partial}{\partial z} P^{x_{n_P}}(Y) & \frac{\partial}{\partial t} P^{x_{n_P}}(Y) \end{array}  \right)\bigg|_{\begin{array}{c}Y = (\Phi(X,t),t) \\ = (x,t) \end{array}} \cdot \left( \begin{array}{c} \frac{\partial}{\partial t} \Phi^{x}(X,t) \\ \frac{\partial}{\partial t} \Phi^{y}(X,t) \\  \frac{\partial}{\partial t} \Phi^{z}(X,t) \\  \frac{\partial}{\partial t} t  \end{array} \right) \nonumber \\
= \left( \begin{array}{cc} \nabla P(x,t) & \frac{\partial}{\partial t} P(x,t) \end{array} \right) \cdot \left( \begin{array}{c} u(x,t) \\ 1 \end{array} \right) \nonumber \\
= (u(x,t) \cdot \nabla) P(x,t) + \frac{\partial}{\partial t} P(x,t)
\end{gather}

The first term of the equation is called convective rate of change, and the second term is called local rate of change. While the material derivative describes the rate of change of the property of the particle with initial position $X$, the local rate of change $\frac{\partial}{\partial t} P(x,t)$ measures the rate of change of $P$ measured at position $x$ at time $t$. Not only does equation ~\ref{eq:nse_eulerderiv} show, that the material derivative is different from the local rate of change, an example can also confirm the difference: Imagine each particle has a temperature $T$ remaining constant over all the time. Obviously the material derivative is zero. But if particles with different temperatures cross position $x$, the local rate of change changes to non-zero values, as changes in temperature are observed at location $x$. Now the label convective rate of change becomes obvious, as it substracts the rate of change purely caused by convection from the local rate of change in equation ~\ref{eq:nse_eulerderiv}.

With the Eulerian formulation of the material derivative, equations ~\ref{eq:nse_materialu} can be rewritten without the trajectory term

\begin{equation}
\label{eq:nse_newtoneuler}
\frac{\partial}{\partial t} u = - (u \cdot \nabla) u + \lim_{\Delta V \rightarrow 0} \frac{\sum_i F_i}{\rho \cdot \Delta V}
\end{equation}

, leaving us with a description of the relation between continuum deformation and the family of forcefields $F_i$ applied to it.

To achieve describing fluid mechanics, we have to work out, what the family of forces $F_i$ looks like.

\section{The Navier Stokes equations}

The Navier Stokes takes internal forces $F_i$ caused by the interaction of the particles into consideration. Of course the right hand side of equation ~\ref{eq:nse_newtoneuler} leaves he inclusion of an additional forcefield $F_a$, like for instance gravity, into the Navier Stokes equations open. Hence the Navier Stoke equations are of the form

\begin{equation}
\label{eq:nse_accelerations}
\frac{\partial}{\partial t} = -(u \cdot \nabla) u + \lim_{\Delta V \rightarrow 0} \frac{F_i}{\rho \cdot \Delta V} + \lim_{\Delta V \rightarrow 0} \frac{F_a}{\rho \cdot \Delta V}
\end{equation}

\subsection{Internal forces}

To derive acceleration $\lim_{\Delta V \rightarrow 0} \frac{F_i}{\rho \cdot \Delta V}$ of a particle at position $Y$ at time $t$, we will work out, what $F_i$ looks like for an axis-alighted fluid volume with edge lengths $\Delta x$, $\Delta y$, $\Delta$ with $Y$ in its center filled with homogenous mass of density $\rho(Y,t)$. The surface of the volume is, where the interaction of the volume with the rest of the fluid is happening, such that $F_i$ is the sum of the faces' stress vectors\footnote{Stress is the force per unit area. The Stress vector of a surface is a way to describe the force acting on it independendly from the area of the surface. Obviously it's the only way to describe the "`force"' acting on surface elements, as the force acting on an area converges to zero, if the area converges to zero.} times the area of those faces. Hence a function $T$ is of interest, which maps a surface normal to the stress vector at $Y$ induced by the other side of the the surface. Cauchy observed that $T$ is a linear mapping. Hence $T$ can be expressed by a matrix

\begin{equation}
\label{eq:if_stresstensor}
T = \left( \begin{array}{ccc} \sigma_{x,x} & \sigma_{y,x} & \sigma_{z,x} \\ \sigma_{x,y} & \sigma_{y,y} & \sigma_{z,y} \\ \sigma_{x,z} & \sigma{y,z} & \sigma{z,z} \end{array} \right)
\end{equation}

, which is called stress tensor. The $i$. column of the stress tensor is obviously equal to the stress vector of the surface with surfacenormal $e_i$. The indices of $\sigma_{i,j}$ are choosen in a way such that index $i$ labels the surface normal of the surface of the stress vector to which the component $\sigma{i,j}$ belongs, and $j$ labels the index in this stress vector.

\begin{figure}[h]
\begin{center}
\includegraphics[height=9cm]{images/stresstensor.png}
\end{center}
\caption{Display of the stress vectors specified by the stress tensor. A red vector is the normal vector on a cut pointing outward from the face, on which $Y$ lies. The stresses $\sigma{i,j}$ are the components of stress vector $T(e_i)$.}
\label{fig:if_stresstensor_pic}
\end{figure}

Assume that $T$ is the stress tensor at $Y$. The $j$. component of the stress vector at the face of the volume with surfacenormal $e_i$ pointing outward can be linearly approximated by $\sigma_{i,j} + \frac{\Delta x_i}{2} \cdot \frac{\partial}{\partial x_i} \sigma_{i,j}$, as the distance between $Y$ and the surface above $Y$ (with $e_i$ pointing upwards) is equal to $\frac{\Delta x_i}{2}$. Shifting the surface perpendicular to $e_i$ from $Y$ for a distance of $-\frac{\Delta x_i}{2}$ would result in a surface with surface stress $\sigma_{i,j} - \frac{\Delta x_i}{2} \cdot \frac{\partial}{\partial x_i} \sigma_{i,j}$. Changing the sign of this term retrieves the surface stress of the volume's face with $-e_i$, as it's the other side of the shifted surface, and $T \cdot (-n) = - T \cdot n$ holds. Multiplying the surface stresses with its face's areas is equal to the force acting on those surfaces by definition. Hence

\begin{equation}
\label{eq:if_forcecomponent}
f_{i,j} = (\sigma_{i,j} + \frac{\Delta x_i}{2} \cdot \frac{\partial}{\partial x_i} \sigma_{i,j}) \cdot \prod_{k \neq i} \Delta x_k - (\sigma_{i,j} - \frac{\Delta x_i}{2} \cdot \frac{\partial}{\partial x_i} \sigma_{i,j}) \cdot \prod_{k \neq i} \Delta x_k = \prod_{k} \Delta x_k \cdot \frac{\partial}{\partial x_i} \sigma_{i,j}
\end{equation}

is the force's $j$. component working on the two volume's faces perpendicular to $e_i$.

\begin{figure}[h]
\begin{center}
\includegraphics[height=9cm]{images/tension.png}
\end{center}
\caption{The surface stresses on two sides of a plain cancel each other out, but the changes of stress $\sigma_{i,j}$ due to the shift along the $x_i$-axis of two parallel faces of the fluid volume contribute to the sum of all stresses acting on the volume.}
\label{fig:if_tension_pic}
\end{figure}

The internal force $F_i$ is the sum all those forces:

\begin{equation}
\label{eq:if_if}
F_i = \sum_{i,j=1}^3 e_j \cdot f_{i,j} = \Delta x \cdot \Delta y \cdot \Delta z \cdot \left( \begin{array}{c} \frac{\partial}{\partial x} \sigma_{x,x} + \frac{\partial}{\partial y}\sigma_{y,x} + \frac{\partial}{\partial z} \sigma_{z,x} \\ \frac{\partial}{\partial x} \sigma_{x,y} + \frac{\partial}{\partial y}\sigma_{y,y} + \frac{\partial}{\partial z} \sigma_{z,y} \\ \frac{\partial}{\partial x} \sigma_{x,z} + \frac{\partial}{\partial y}\sigma_{y,z} + \frac{\partial}{\partial z} \sigma_{z,z}  \end{array} \right) = \Delta V \cdot T \cdot \nabla
\end{equation}

Now the acceleration of the particle at $Y$, which we wanted to compute in the first place can be calculated:

\begin{equation}
\label{eq:if_acceleration}
\lim_{\Delta V \rightarrow 0} \frac{F_i}{\rho \cdot \Delta V} = \frac{1}{\rho} \cdot T \cdot \nabla
\end{equation}

The Navier Stokes equations assume two kinds of interactions between the fluid particles: Repulsion and friction. The stress caused by repulsion is called hydrostatic stress, and only acts perpendicular and inwards a fluid volume, as it's a frictionless stress, which doesn't cause any shear stresses. As a result the internal force can be separated into a hydrostatic part and a friction part. Let $T_p$ the stress tensor specifying the pressure, then

\begin{gather}
\label{eq:if_separatetensor}
T = T_p + (T - T_p) = T_p + \sigma \\
\sigma = T - T_p
\end{gather}

holds. $T_p$ is called the hydrostatic stress tensor, and $\sigma$ is called deviatoric stress tensor, describing the stress solely caused by friction of the fluid particles. Due to $T \cdot \nabla = T_p \cdot \nabla + \sigma \cdot \nabla$, the acceleration in ~\ref{eq:if_acceleration} is the sum of two accelerations:

\begin{gather}
\label{eq:if_separateacceleration}
\lim_{\Delta V \rightarrow 0} \frac{F_i}{\rho \cdot \Delta V} = \frac{1}{\rho} \cdot T_p \cdot \nabla + \frac{1}{\rho} \cdot \sigma \cdot \nabla
\end{gather}

\subsubsection{Pressure}

Due to the fact that hydrostatic stress does not cause shear stresses, the hydrostatic stress tensor $T_p$ forms a diagonal matrix. Furthermore it can be observed that hydrostatic stress doesn't depend on the direction of the surface it acts on. Hence the entries in the diagonal of the matrix are equal. Let $p$ be the negative of a diagonal value of $T_p$, such that

\begin{equation}
\label{eq:p_hst}
T_p = \left( \begin{array}{ccc} -p & 0 & 0 \\ 0 & -p & 0 \\ 0 & 0 & -p \end{array} \right)
\end{equation}

holds, then the scalarfield

\begin{equation}
\label{eq:p_p}
p : \Omega \times \mathbb{R} \rightarrow \mathbb{R} : (x,t) \mapsto p(x,t)
\end{equation}

is called pressure. According to ~\ref{eq:if_separateacceleration} the contribution of $T_p$ to the acceleration is given by

\begin{equation}
\label{eq:p_acceleration}
\frac{1}{\rho} \cdot T_p \cdot \nabla = -\frac{1}{\rho} \dot \nabla p
\end{equation}

\subsubsection{Viscosity}

As mentioned before, the surface stresses specified by the deviatoric stress tensor $\sigma$ are explained only in terms of the friction between the fluid particles. Consider the surface with area $A$ of a fluid volume perpendicular to $x_i$ flowing into the $x_j$ direction at speed $u_0$, where each fluid layer with distance $d$ to the surface above it moves at speed $u_d = u_0 + u \cdot d$ into $x_j$ direction, then a force $F$ pointing in the direction of the flow change is caused, which is proportional to the area of the surface, to $u_d - u_0$, and indirect proportional to $d$. Hence

\begin{equation}
\label{eq:vis_feq}
F \propto \frac{ A \cdot (u_d - u_0)}{d}
\end{equation}

holds.

\begin{figure}[h]
\begin{center}
\includegraphics[height=7cm]{images/viscosity.png}
\end{center}
\caption{The force caused by fluid deformation is proportional to the relative speed of a fluid layer and its area, and indirect proportional to the distance to this layer.}
\label{fig:vis_feq_pic}
\end{figure}

In Newtonian fluids the factor of the right hand side to turn ~\ref{eq:vis_feq} into an equation does not depend on the speed difference $u_d - u_0$, hence a constant $\upsilon$ exists, such that we optain equation

\begin{equation}
\label{eq:vis_visnewton}
F = \upsilon \cdot \frac{ A \cdot (u_d - u_0)}{d}
\end{equation}

Constant $\upsilon$ is called the viscosity of the fluid. By dividing both sides of the equation by area $A$, and letting d converge to zero we obtain Newton's law of viscosity

\begin{equation}
\label{eq:vis_vislaw}
\tau = \upsilon \cdot \frac{\partial}{\partial x_i} u^j
\end{equation}

$\tau$ is the stress caused in $x_j$ direction by the deformation of the flow along the $x_i$-axis. The surface stresses in the deviatoric stress tensor $\sigma$ exactly correspond to equation ~\ref{eq:vis_vislaw}. Let $\sigma$ be

\begin{equation}
\label{eq:vis_tensor}
\sigma = \left( \begin{array}{ccc} \tau_{x,x} & \tau_{y,x} & \tau_{z,x} \\ \tau_{x,y} & \tau_{y,y} & \tau_{z,y} \\ \tau_{x,z} & \tau_{y,z} & \tau_{z,z} \end{array} \right)
\end{equation}

As $\tau_{i,j}$ is the $j$. component of the surface stress with surface normal $e_i$, Newton's law of viscosity implies

\begin{equation}
\label{eq:vis_tau}
\tau_{i,j} = \upsilon \cdot \frac{\partial}{\partial x_i} u^j
\end{equation}

So the deviatoric stress tensor is

\begin{equation}
\label{eq_vis_tensorentries}
\sigma = \upsilon \cdot \left( \begin{array}{c} \nabla u^x \\ \nabla u^y \\ \nabla u^z \end{array} \right)
\end{equation}

According to ~\ref{eq:if_separateacceleration} the contribution of $\sigma$ to the acceleration is given by

\begin{equation}
\label{eq:p_acceleration}
\frac{1}{\rho} \cdot \sigma \cdot \nabla = \frac{\upsilon}{\rho} \dot \nabla^2 u
\end{equation}

But a very important constraint to ensure a physical correct solution is still missing. For any density $\rho$ fulfilling the above equations, $\rho(x,t) + g(t)$ with an arbitrary function $g(t)$ in time would be also a solution. This obviously has to be false, as the mass of the fluid must remain constant over time.

\subsection{Continuity equation}

In classical Physics the mass of a closed system can't change over time. Hence the rate of change of a volumeset $V$'s mass $m$ has to be equal to the amount of mass per time unit entering the volume through the surface $\partial V$.

\begin{figure}[h]
\begin{center}
\includegraphics[height=10cm]{images/continuity.png}
\end{center}
\caption{The rate of mass change of volume V equals the mass per second flowing through the surface $\partial V$.}
\label{fig:cont_enter_pic}
\end{figure}

Suppose fluid of density $\rho$ is entering the volume through a surface element $\Delta ^ 2 S$ with outwardpointing unit normal $n$. Then the fluid volume entering V through $\Delta ^ 2 S$ is equal to the volume of the cuboid of height $(-n) \cdot u$, and a bottom area of $\Delta ^ 2 S$. So the mass of the fluid volume flowing through the surface element equals $\rho \cdot (-n) \cdot u \cdot \Delta ^2$. Integration over all surface elements equals the rate of mass change:

\begin{equation}
\label{eq:cont_enter}
\frac{\partial}{\partial t} m = \oint_{\partial V} \cdot (-n) \cdot (u \cdot \rho) \, d^2 S = \int_{V} -\nabla \cdot (u \cdot \rho) \, d^3 V
\end{equation}

The right hand side of the equation is obtained by application of the divergence theorem. Additionally we take the definition of $\rho$ as the the density function of $m$ into consideration:

\begin{equation}
\label{eq:cont_density}
\frac{\partial}{\partial t} m = \frac{\partial}{\partial t} \int_{V} \rho \, d^3 V = \int_{V} \frac{\partial}{\partial t} \rho \, d^3 V
\end{equation}

The equality of the right hand sides of equations ~\ref{eq:cont_enter} and ~\ref{eq:cont_density} implies that their integrands are equal. Hence we can derive a continuity equation of mass conservation

\begin{equation}
\label{eq:cont_continuity}
\frac{\partial}{\partial t} \rho + \nabla \cdot (\rho \cdot u) = 0
\end{equation}

Equations ~\ref{eq:nse_accelerations}, ~\ref{eq:p_acceleration}, ~\ref{eq:p_acceleration} and ~\ref{eq:cont_continuity} lead to the Navier Stokes equations

\begin{gather}
\frac{\partial}{\partial t} u = - (u \cdot \nabla) u + \frac{1}{\rho} \cdot (-\nabla p + \upsilon \cdot \nabla^2 u) \label{eq:navier_navier} \\
\frac{\partial}{\partial t} \rho + \nabla \cdot (\rho \cdot u) = 0 \label{eq:navier_continuity}
\end{gather}

For incompressible fluids additionally the constraint $\rho = const$ is added, which together with the continuity equation is equal to

\begin{equation}
\label{eq:navier_incompressible}
\nabla \cdot u = 0
\end{equation}

Hence the constraint ~\ref{eq:navier_incompressible} can replace both the continuity equation and the constraint $\rho = const$ in the Navier Stokes equations.