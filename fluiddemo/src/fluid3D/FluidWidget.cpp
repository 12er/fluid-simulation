#ifndef _FLUID3D_FLUIDWIDGET_CPP_
#define _FLUID3D_FLUIDWIDGET_CPP_

#include <Fluid3D.h>
#include <QGridLayout>
#include <QLabel>
#include <QComboBox>
#include <QFontMetrics>
#include <QApplication>
#include <sx/SXWidget.h>

namespace fluid {

	const QString FluidWidget::algorithmCombinedAdvect = "combined advect";
	const QString FluidWidget::algorithmForwardEuler = "forward euler advect";
	const QString FluidWidget::algorithmMacCormack = "mac cormack advect";

	const QString FluidWidget::runSimulation = "run simulation";
	const QString FluidWidget::stopSimulation = "stop simulation";

	const QString FluidWidget::statusRunning = "simulation is running";
	const QString FluidWidget::statusPause = "simulation stopped";

	const QString FluidWidget::format_float16 = "16 bit float";
	const QString FluidWidget::format_float32 = "32 bit float";

	void FluidWidget::resendSetAlgorithm() {
		QString selection = algorithmSettings->currentText();
		if(selection.compare(algorithmCombinedAdvect) == 0) {
			setAlgorithm(FLUID_COMBINEDADVECT);
		} else if(selection.compare(algorithmForwardEuler) == 0) {
			setAlgorithm(FLUID_FORWARDEULER);
		} else if(selection.compare(algorithmMacCormack) == 0) {
			setAlgorithm(FLUID_MACCORMACK);
		}
	}

	void FluidWidget::resendSetRunSimulation() {
		if(runSimulationButton->text().compare(runSimulation) == 0) {
			runSimulationButton->setText(stopSimulation);
			setRunSimulation(true);
		} else if(runSimulationButton->text().compare(stopSimulation) == 0) {
			runSimulationButton->setText(runSimulation);
			setRunSimulation(false);
		}
	}

	void FluidWidget::resendResetSimulation() {
		resetSimulation();
	}

	void FluidWidget::resendResetData() {
		resetData();
	}

	void FluidWidget::resendResetFluid() {
		resetFluid();
	}

	void FluidWidget::showRunSimulation() {
		runSimulationButton->setText(stopSimulation);
	}

	void FluidWidget::showPauseSimulation() {
		runSimulationButton->setText(runSimulation);
	}

	void FluidWidget::resendSetGridDimensions() {

		bool wok;
		bool hok;
		bool dok;
		int width = widthField->text().toInt(&wok);
		int height = heightField->text().toInt(&hok);
		int depth = depthField->text().toInt(&dok);
		PixelFormat format = FLOAT_RGBA;
		QString fmt = formatCombo->currentText();

		if(fmt.compare(format_float16) == 0) {
			format = FLOAT16_RGBA;
		} else if(fmt.compare(format_float32) == 0) {
			format = FLOAT_RGBA;
		}

		if(width > 0 && height > 0 && depth > 0 && wok && hok && dok) {
			setGridDimensions(width,height,depth,format);
		} else {
			stringstream wstr;
			stringstream hstr;
			stringstream dstr;
			wstr << solver->getGridWidth();
			hstr << solver->getGridHeight();
			dstr << solver->getGridDepth();
			widthField->setText(QString(wstr.str().c_str()));
			heightField->setText(QString(hstr.str().c_str()));
			depthField->setText(QString(dstr.str().c_str()));
		}
	}

	void FluidWidget::resendSetDX() {
		bool take;
		float dx = dxField->text().toFloat(&take);
		if(dx > 0.0f && take) {
			setDX(dx);
		} else {
			stringstream sstr;
			sstr << solver->getDX();
			dxField->setText(QString(sstr.str().c_str()));
		}
	}

	FluidWidget::FluidWidget(): QWidget() {

		QGridLayout *grid = new QGridLayout();
		this->setLayout(grid);

		//visualization of simulation
		solver = new fluid::FluidSolver();
		sx::SXWidget *widget = new sx::SXWidget();
		widget->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
		widget->setWindowTitle("ShadeXEngine 3D fluid demo");
		
		widget->setMinimumSize(984, 560);
		widget->renderPeriodically(0.01f);
		widget->addRenderListener(*solver);
		QObject::connect(widget,SIGNAL(finishedRendering()),this,SLOT(close()));
		QObject::connect(this,SIGNAL(setAlgorithm(FluidAlgorithm)),solver,SLOT(setAlgorithm(FluidAlgorithm)));
		QObject::connect(this,SIGNAL(setRunSimulation(bool)),solver,SLOT(setRunSimulation(bool)));
		QObject::connect(this,SIGNAL(resetSimulation()),solver,SLOT(resetSimulation()));
		QObject::connect(this,SIGNAL(resetData()),solver,SLOT(resetData()));
		QObject::connect(this,SIGNAL(resetFluid()),solver,SLOT(resetFluid()));
		QObject::connect(this,SIGNAL(setGridDimensions(int,int,int,int)),solver,SLOT(setGridDimensions(int,int,int,int)));
		QObject::connect(this,SIGNAL(setDX(float)),solver,SLOT(setDX(float)));
		QObject::connect(solver,SIGNAL(isRunningSimulation()),this,SLOT(showRunSimulation()));
		QObject::connect(solver,SIGNAL(isPausingSimulation()),this,SLOT(showPauseSimulation()));
		grid->addWidget(widget,0,0);

		//settings widget
		QWidget *settingsWidget = new QWidget();
		settingsWidget->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		QGridLayout *settingsGrid = new QGridLayout();
		settingsWidget->setLayout(settingsGrid);

		QWidget *algorithmWidget = new QWidget();
		algorithmWidget->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		QGridLayout *algorithmGrid = new QGridLayout();
		algorithmWidget->setLayout(algorithmGrid);
		algorithmSettings = new QComboBox();
		algorithmSettings->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		QLabel *algorithmLabel = new QLabel("algorithm");
		algorithmLabel->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		algorithmGrid->addWidget(algorithmSettings,0,1);
		algorithmGrid->addWidget(algorithmLabel,0,0);
		QStringList algorithmList;
		algorithmList << algorithmMacCormack << algorithmCombinedAdvect << algorithmForwardEuler;
		algorithmSettings->addItems(algorithmList);
		settingsGrid->addWidget(algorithmWidget,0,0);

		runSimulationButton = new QPushButton(stopSimulation);
		runSimulationButton->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		settingsGrid->addWidget(runSimulationButton,1,0);

		QPushButton *resetSimulationButton = new QPushButton("reset whole simulation");
		resetSimulationButton->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		settingsGrid->addWidget(resetSimulationButton,2,0);

		QPushButton *resetDataButton = new QPushButton("reset colors");
		resetDataButton->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		settingsGrid->addWidget(resetDataButton,3,0);

		QPushButton *resetFluidButton = new QPushButton("reset velocity and pressure");
		resetFluidButton->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		settingsGrid->addWidget(resetFluidButton,4,0);

		QFontMetrics metrics(QApplication::font());

		QWidget *dimensionsWidget = new QWidget();
		dimensionsWidget->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		QGridLayout *dimensionsGrid = new QGridLayout();
		dimensionsWidget->setLayout(dimensionsGrid);
		widthField = new QLineEdit("160");
		widthField->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		widthField->setFixedWidth(metrics.width("WWWWW"));
		heightField = new QLineEdit("160");
		heightField->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		heightField->setFixedWidth(metrics.width("WWWWW"));
		depthField = new QLineEdit("160");
		depthField->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		depthField->setFixedWidth(metrics.width("WWWWW"));
		QStringList formatsList;
		formatsList << format_float16 << format_float32;
		formatCombo = new QComboBox();
		formatCombo->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		formatCombo->addItems(formatsList);
		QLabel *widthLabel = new QLabel("width");
		widthLabel->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		QLabel *heightLabel = new QLabel("height");
		heightLabel->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		QLabel *depthLabel = new QLabel("depth");
		depthLabel->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		QLabel *formatLabel = new QLabel("pixelformat");
		dimensionsGrid->addWidget(widthField,0,1);
		dimensionsGrid->addWidget(heightField,1,1);
		dimensionsGrid->addWidget(depthField,2,1);
		dimensionsGrid->addWidget(formatCombo,3,1);
		dimensionsGrid->addWidget(widthLabel,0,0);
		dimensionsGrid->addWidget(heightLabel,1,0);
		dimensionsGrid->addWidget(depthLabel,2,0);
		dimensionsGrid->addWidget(formatLabel,3,0);
		QPushButton *dimensionsButton = new QPushButton("set grid dimensions");
		dimensionsButton->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		dimensionsGrid->addWidget(dimensionsButton,4,0,1,2);
		settingsGrid->addWidget(dimensionsWidget,5,0);

		QWidget *dxWidget = new QWidget();
		dxWidget->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		QGridLayout *dxGrid = new QGridLayout();
		dxWidget->setLayout(dxGrid);
		dxField = new QLineEdit("0.01");
		dxField->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		dxField->setFixedWidth(metrics.width("WWWWW"));
		QLabel *dxLabel = new QLabel("dx");
		dxLabel->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		QPushButton *dxButton = new QPushButton("set dx");
		dxButton->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		dxGrid->addWidget(dxField,0,1);
		dxGrid->addWidget(dxLabel,0,0);
		dxGrid->addWidget(dxButton,1,0,1,2);
		settingsGrid->addWidget(dxWidget);
		
		QObject::connect(algorithmSettings,SIGNAL(currentIndexChanged(int)),this,SLOT(resendSetAlgorithm()));
		QObject::connect(runSimulationButton,SIGNAL(clicked()),this,SLOT(resendSetRunSimulation()));
		QObject::connect(resetSimulationButton,SIGNAL(clicked()),this,SLOT(resendResetSimulation()));
		QObject::connect(resetDataButton,SIGNAL(clicked()),this,SLOT(resendResetData()));
		QObject::connect(resetFluidButton,SIGNAL(clicked()),this,SLOT(resendResetFluid()));
		QObject::connect(dimensionsButton,SIGNAL(clicked()),this,SLOT(resendSetGridDimensions()));
		QObject::connect(dxButton,SIGNAL(clicked()),this,SLOT(resendSetDX()));
		grid->addWidget(settingsWidget,0,1,Qt::AlignTop | Qt::AlignLeft);
	}

}

#endif