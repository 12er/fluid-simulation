#ifndef _FLUID3D_FLUIDSOLVER_CPP_
#define _FLUID3D_FLUIDSOLVER_CPP_

#include <sx/Log4SX.h>
#include <sx/Exception.h>
#include <ShadeX.h>
#include <Fluid3D.h>
using namespace sx;

namespace fluid {

	FluidSolver::FluidSolver(): QObject() {
		lastMouseX = lastMouseY = -1;
	}

	FluidSolver::~FluidSolver() {
	}

	void FluidSolver::setAlgorithm(FluidAlgorithm algorithm) {
		this->algorithm = algorithm;
	}

	void FluidSolver::setRunSimulation(bool run) {
		this->pauseSimulation = !run;
	}

	void FluidSolver::resetSimulation() {
		Effect &copystart = SX::shadeX.getEffect("copystart.effect");
		copystart.render();
	}

	void FluidSolver::resetData() {
		Effect &reset = SX::shadeX.getEffect("resetdata.effect");
		reset.render();
	}

	void FluidSolver::resetFluid() {
		Effect &reset = SX::shadeX.getEffect("resetfluid.effect");
		reset.render();
	}

	void FluidSolver::setGridDimensions(int width, int height, int depth, int format) {
		Volume &data = SX::shadeX.getVolume("data.volume");
		Volume &data2 = SX::shadeX.getVolume("data2.volume");
		Volume &velocitypressure = SX::shadeX.getVolume("velocitypressure.volume");
		Volume &velocitypressure2 = SX::shadeX.getVolume("velocitypressure2.volume");
		Volume &velocitypressure3 = SX::shadeX.getVolume("velocitypressure3.volume");
		RenderTarget &voltarget = SX::shadeX.getRenderTarget("volume.target");
		data.setWidth(width);
		data.setHeight(height);
		data.setDepth(depth);
		data.setPixelFormat(BYTE_RGBA);
		data2.setWidth(width);
		data2.setHeight(height);
		data2.setDepth(depth);
		data2.setPixelFormat(BYTE_RGBA);

		velocitypressure.setWidth(width);
		velocitypressure.setHeight(height);
		velocitypressure.setDepth(depth);
		velocitypressure.setPixelFormat((PixelFormat)format);

		velocitypressure2.setWidth(width);
		velocitypressure2.setHeight(height);
		velocitypressure2.setDepth(depth);
		velocitypressure2.setPixelFormat((PixelFormat)format);

		velocitypressure3.setWidth(width);
		velocitypressure3.setHeight(height);
		velocitypressure3.setDepth(depth);
		velocitypressure3.setPixelFormat((PixelFormat)format);

		voltarget.setWidth(width);
		voltarget.setHeight(height);
		voltarget.setRenderToDisplay(false);

		data.load();
		data2.load();
		velocitypressure.load();
		velocitypressure2.load();
		velocitypressure3.load();
		voltarget.load();

		Effect &effect = SX::shadeX.getEffect("copystart.effect");
		effect.render();
	}

	void FluidSolver::setDX(float dx) {
		UniformFloat &d = SX::shadeX.getUniformFloat("dx.float");
		d << dx;
	}

	void FluidSolver::create(SXRenderArea &area) {
		this->width = area.getWidth();
		this->height = area.getHeight();

		this->holdMouse = false;
		this->grabbing = false;

		this->pauseSimulation = false;
		this->pressedSpace = false;

		this->algorithm = FLUID_MACCORMACK;

		eye = Vector(0.65f,0.65f,0.65f);
		view = Vector(-1,-1,-1);
		up = Vector(0,0,1);

		try {
			SX::shadeX.addResources("../data/3D/effect.c");
		} catch(Exception &e) {
			Logger::get() << LogMarkup("Demo::create") << e.getMessage();
			area.stopRendering();
			return;
		}

		try {
			SX::shadeX.load();
		} catch(Exception &e) {
			Logger::get() << LogMarkup("Demo::create") << e.getMessage();
		}

		Effect &copystart = SX::shadeX.getEffect("copystart.effect");
		copystart.render();
	}

	void FluidSolver::reshape(SXRenderArea &area) {
		this->width = area.getWidth();
		this->height = area.getHeight();

		RenderTarget &display = SX::shadeX.getRenderTarget("mainscene.display");
		display.setWidth(width);
		display.setHeight(height);
		display.load();
	}

	void FluidSolver::render (SXRenderArea &area) {
		if(area.hasMouseKey(MOUSE_RIGHT)) {
			//right mousebutton is used to rotate camera
			if(!holdMouse) {
				holdMouseX = area.getMouseX();
				holdMouseY = area.getMouseY();
				holdMouse = true;
			}
			area.setMousePointer(holdMouseX,holdMouseY);
		} else {
			holdMouse = false;
		}

		UniformFloat &deltatime = SX::shadeX.getUniformFloat("deltatime.float");
		//deltatime = (float)area.getDeltaTime() * 4.5f;

		//calculate cameradata, and transfer the outcome to shadeX
		//w,s,a,d control for the position
		float forward = 0;
		float right = 0;
		float speed = 1.0f;
		if(area.hasKey('W') && !area.hasKey('S')) {
			//move forward
			forward = (float)area.getDeltaTime();
		} else if(!area.hasKey('W') && area.hasKey('S')) {
			//move backward
			forward = (float)-area.getDeltaTime();
		}
		if(area.hasKey('D') && !area.hasKey('A')) {
			//move right
			right = (float)area.getDeltaTime();
			
		} else if(!area.hasKey('D') && area.hasKey('A')) {
			//move left
			right = (float)-area.getDeltaTime();
		}
		Vector vRight = view % Vector(0,0,1);
		eye = eye + view.normalize() * forward * speed + vRight.normalize() * right * speed;
		
		//mousemovement control
		if(area.hasMouseKey(MOUSE_RIGHT)) {
			//press right button
			// -> rotate camera
			float rotSpeed = 1.0f;
			Vector rotViewerDir = Vector(
				(float)area.getMouseDeltaX(),
				(float)area.getMouseDeltaY()
				);
			rotViewerDir.scalarmult((float)area.getDeltaTime());
			view = view.normalize() + vRight * rotViewerDir[0] + Vector(0,0,1) * rotViewerDir[1];
			view.normalize();
		}

		Matrix &viewMatrix = SX::shadeX.getUniformMatrix("mainscene.camera.view");
		Matrix &viewInvMatrix = SX::shadeX.getUniformMatrix("mainscene.camera.viewinv");
		Matrix &projMatrix = SX::shadeX.getUniformMatrix("mainscene.camera.projection");
		Matrix &projInvMatrix = SX::shadeX.getUniformMatrix("mainscene.camera.projectioninv");
		Vector &eyeVector = SX::shadeX.getUniformVector("mainscene.camera.eye");
		eyeVector = eye;
		viewMatrix = Matrix().viewMatrix(eye,view,Vector(0,0,1));
		viewInvMatrix = viewMatrix;
		viewInvMatrix.inverse();
		projMatrix = Matrix().perspectiveMatrix((float)Tau*0.25f,(float)area.getWidth(),(float)area.getHeight(),0.1f,50.0f);
		projInvMatrix = projMatrix;
		projInvMatrix.inverse();

		//mouse moving fluidpart around
		if(area.hasMouseKey(MOUSE_LEFT)) {
			Vector projPos((float)area.getMouseX(),(float)area.getMouseY(),-1.0f);
			projPos[0] = projPos[0] * 2.0f / (float)area.getWidth() - 1.0f;
			projPos[1] = projPos[1] * 2.0f / (float)area.getHeight() - 1.0f;

			Vector worldPos;
			worldPos = viewInvMatrix * projInvMatrix * projPos;
			worldPos = Vector(worldPos[0]/worldPos[3],worldPos[1]/worldPos[3],worldPos[2]/worldPos[3],1);

			Vector ray = eyeVector * (-1.0f);
			ray = worldPos + ray;
			ray.normalize();
			
			worldPos = worldPos + ray;

			Vector texPos = Matrix(
					0.5f,	0,		0,		0.5f,
					0,		0.5f,	0,		0.5f,
					0,		0,		0.5f,	0.5f,
					0,		0,		0,		1
				) * worldPos;

			if(!grabbing) {
				oldGrabPos = texPos;
				grabbing = true;
			}

			Vector &grabPosVector = SX::shadeX.getUniformVector("grabbing.grabPosition");
			Vector &grabVelVector = SX::shadeX.getUniformVector("grabbing.grabVelocity");
			grabPosVector = texPos;
			grabVelVector = (texPos + oldGrabPos * (-1.0f)) * 100.0f;
			oldGrabPos = texPos;

			Effect &grabEffect = SX::shadeX.getEffect("grabbing.effect");
			grabEffect.render();
		} else {
			grabbing = false;
		}

		if(area.hasKey(' ')) {
			if(!pressedSpace) {
				pauseSimulation = !pauseSimulation;
				if(pauseSimulation) {
					isPausingSimulation();
				} else {
					isRunningSimulation();
				}
				pressedSpace = true;
			}
		} else {
			pressedSpace = false;
		}
		if(!pauseSimulation) {
			if(algorithm == FLUID_COMBINEDADVECT) {
				Effect &simulation = SX::shadeX.getEffect("combinedjacobiadvection.effect");
				simulation.render();
			} else if(algorithm == FLUID_FORWARDEULER) {
				Effect &simulation = SX::shadeX.getEffect("forwardeuler.effect");
				simulation.render();
			} else if(algorithm == FLUID_MACCORMACK) {
				Effect &simulation = SX::shadeX.getEffect("maccormack.effect");
				simulation.render();
			}
		}

		Effect &effect = SX::shadeX.getEffect("mainscene.effect");
		effect.render();

		if(area.hasKey('D') && area.hasKey(SX_BACKSPACE)) {
			//copy back start dataset
			Effect &reset = SX::shadeX.getEffect("reset.data.effect");
			reset.render();
		}
		
		if(area.hasKey('V') && area.hasKey(SX_BACKSPACE)) {
			//copy back start velocity
			Effect &reset = SX::shadeX.getEffect("reset.velocity.effect");
			reset.render();
		}
		
		if(area.hasKey('P') && area.hasKey(SX_BACKSPACE)) {
			//copy back start pressure
			Effect &reset = SX::shadeX.getEffect("reset.pressure.effect");
			reset.render();
		}
		
		if(area.hasKey('A') && area.hasKey(SX_BACKSPACE)) {
			//copy back initial state
			Effect &reset = SX::shadeX.getEffect("reset.effect");
			reset.render();
		}

		if(area.hasKey('V') && area.hasKey(' ')) {
			//damp velocity
			Effect &damping = SX::shadeX.getEffect("damping.velocity.effect");
			damping.render();
		}
		
		if(area.hasKey('P') && area.hasKey(' ')) {
			//damp velocity
			Effect &damping = SX::shadeX.getEffect("damping.pressure.effect");
			damping.render();
		}
		
		if(area.hasKey('A') && area.hasKey(' ')) {
			//damp velocity
			Effect &damping = SX::shadeX.getEffect("damping.effect");
			damping.render();
		}

		if(area.hasKey(SX_ESC)) {
			//pressed ESC, quit
			area.stopRendering();
		}
	}

	void FluidSolver::stop(SXRenderArea &area) {
	}

	int FluidSolver::getGridWidth() {
		Volume &v = SX::shadeX.getVolume("data.volume");
		return v.getWidth();
	}
		
	int FluidSolver::getGridHeight() {
		Volume &v = SX::shadeX.getVolume("data.volume");
		return v.getHeight();
	}

	int FluidSolver::getGridDepth() {
		Volume &v = SX::shadeX.getVolume("data.volume");
		return v.getDepth();
	}

	float FluidSolver::getDX() {
		UniformFloat &d = SX::shadeX.getUniformFloat("dx.float");
		float dx = 0.0f;
		d >> dx;
		return dx;
	}

}

#endif