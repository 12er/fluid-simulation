#ifndef _FLUID2D_FLUIDSOLVER_CPP_
#define _FLUID2D_FLUIDSOLVER_CPP_

#include <sx/Log4SX.h>
#include <sx/Exception.h>
#include <ShadeX.h>
#include <Fluid2D.h>
using namespace sx;

namespace fluid {

	FluidSolver::FluidSolver() {
		lastMouseX = lastMouseY = -1;
	}

	FluidSolver::~FluidSolver() {
	}

	void FluidSolver::create(SXRenderArea &area) {
		this->width = area.getWidth();
		this->height = area.getHeight();

		try {
			SX::shadeX.addResources("../data/2D/effect.c");
			SX::shadeX.addResources("../data/2D/resources.c");
		} catch(Exception &e) {
			Logger::get() << LogMarkup("Demo::create") << e.getMessage();
			area.stopRendering();
			return;
		}

		try {
			SX::shadeX.load();
		} catch(Exception &e) {
			Logger::get() << LogMarkup("Demo::create") << e.getMessage();
		}

		Effect &copystart = SX::shadeX.getEffect("copystart.effect");
		copystart.render();
	}

	void FluidSolver::reshape(SXRenderArea &area) {
		this->width = area.getWidth();
		this->height = area.getHeight();

		RenderTarget &display = SX::shadeX.getRenderTarget("mainscene.display");
		display.setWidth(width);
		display.setHeight(height);
		display.load();
	}

	void FluidSolver::render (SXRenderArea &area) {
		UniformFloat &deltatime = SX::shadeX.getUniformFloat("deltatime");
		deltatime = (float)area.getDeltaTime() * 1.0f;

		if(algorithm == FLUID_FORWARDEULER) {
			Effect &effect = SX::shadeX.getEffect("maineffect.forwardeuler");
			effect.render();
		} else if(algorithm == FLUID_MACCORMACK) {
			Effect &effect = SX::shadeX.getEffect("maineffect.maccormack");
			effect.render();
		}

		if(area.hasMouseKey(MOUSE_LEFT)) {
			//apply input
			int mouseX = area.getMouseX();
			int mouseY = area.getMouseY();
			Vector &position = SX::shadeX.getUniformVector("input.position");
			Vector &speed = SX::shadeX.getUniformVector("input.speed");
			position = Vector( (float)mouseX/(float)width ,(float)mouseY/(float)height ) * 2.0f;
			Vector oldp;
			if(lastMouseX == -1) {
				//mousebutton was not pressed last time, so only zero speed
				//for the mousepointer can be assumed
				oldp = position;
			} else {
				oldp = Vector( (float)lastMouseX/(float)width , (float)lastMouseY/(float)height ) * 2.0f;
			}
			speed = position + --oldp;
			if(position[0] > 1.0f) {
				position[0] = position[0] - 1.0f;
			}
			if(position[1] > 1.0f) {
				position[1] = position[1] - 1.0f;
			}
			lastMouseX = mouseX;
			lastMouseY = mouseY;
			//apply input to speedfield
			Effect &takeinput = SX::shadeX.getEffect("input.effect");
			takeinput.render();
		} else {
			//mousebutton released, hence ignore last mouse position the next time
			lastMouseX = lastMouseY = -1;
		}

		if(area.hasKey('D') && area.hasKey(SX_BACKSPACE)) {
			//copy back start dataset
			Effect &reset = SX::shadeX.getEffect("reset.data.effect");
			reset.render();
		}
		
		if(area.hasKey('V') && area.hasKey(SX_BACKSPACE)) {
			//copy back start velocity
			Effect &reset = SX::shadeX.getEffect("reset.velocity.effect");
			reset.render();
		}
		
		if(area.hasKey('P') && area.hasKey(SX_BACKSPACE)) {
			//copy back start pressure
			Effect &reset = SX::shadeX.getEffect("reset.pressure.effect");
			reset.render();
		}
		
		if(area.hasKey('A') && area.hasKey(SX_BACKSPACE)) {
			//copy back initial state
			Effect &reset = SX::shadeX.getEffect("reset.effect");
			reset.render();
		}

		if(area.hasKey('V') && area.hasKey(' ')) {
			//damp velocity
			Effect &damping = SX::shadeX.getEffect("damping.velocity.effect");
			damping.render();
		}
		
		if(area.hasKey('P') && area.hasKey(' ')) {
			//damp velocity
			Effect &damping = SX::shadeX.getEffect("damping.pressure.effect");
			damping.render();
		}
		
		if(area.hasKey('A') && area.hasKey(' ')) {
			//damp velocity
			Effect &damping = SX::shadeX.getEffect("damping.effect");
			damping.render();
		}

		if(area.hasKey('1')) {
			algorithm = FLUID_FORWARDEULER;
		}

		if(area.hasKey('2')) {
			algorithm = FLUID_MACCORMACK;
		}

		if(area.hasKey(SX_ESC)) {
			//pressed ESC, quit
			area.stopRendering();
		}
	}

	void FluidSolver::stop(SXRenderArea &area) {
	}

}

#endif