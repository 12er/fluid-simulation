#ifndef _FLUIDDEMO2D_CPP_
#define _FLUIDDEMO2D_CPP_

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
#include <sx/Log4SX.h>
#include <sx/SX.h>
#include <sx/Exception.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <Fluid2D.h>

/**
 * Entry point of the demo
 */
int main(int argc, char **argv) {
	QApplication app(argc,argv);

	try {
		sx::Logger::addLogger("demo",new sx::FileLogger("FluidDemo2D.html"));
		sx::Logger::setDefaultLogger("demo");
	} catch(sx::Exception &e) {
		sx::Logger::get() << e.getMessage();
	}

	sx::SXWidget widget;
	widget.setWindowTitle("ShadeXEngine 2D fluid demo");
	widget.move(100,100);
	widget.setMinimumSize(984, 560);
	widget.renderPeriodically(0.01f);
	widget.addRenderListener(*new fluid::FluidSolver());
	QObject::connect(&widget,SIGNAL(finishedRendering()),&widget,SLOT(close()));
	try {
		widget.show();
	} catch(sx::Exception &e) {
		sx::Logger::get() << e.getMessage();
	}

	return app.exec();
}

#endif