#ifndef _FLUIDDEMO3D_CPP_
#define _FLUIDDEMO3D_CPP_

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
#include <sx/Log4SX.h>
#include <sx/SX.h>
#include <sx/Exception.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <Fluid3D.h>

/**
 * Entry point of the demo
 */
int main(int argc, char **argv) {
	QApplication app(argc,argv);

	try {
		sx::Logger::addLogger("demo",new sx::FileLogger("FluidDemo3D.html"));
		sx::Logger::setDefaultLogger("demo");
	} catch(sx::Exception &e) {
		sx::Logger::get() << e.getMessage();
	}

	fluid::FluidWidget widget;
	widget.move(100,100);
	try {
		widget.show();
	} catch(sx::Exception &e) {
		sx::Logger::get() << e.getMessage();
	}

	return app.exec();
}

#endif