#ifndef _SX_EXCEPTION_H_
#define _SX_EXCEPTION_H_

/**
 * Exception class
 * (c) 2012 by Tristan Bauer
 */
#include <export/Export.h>
#include <exception>
#include <string>
using namespace std;

namespace sx {

	/**
	 * types of parser exception
	 */
	enum parserException {
		EX_IO ,
		EX_SYNTAX ,
		EX_COMPILE ,
		EX_NODATA ,
		EX_AMBIGUOUS ,
		EX_BOUNDS ,
		EX_INIT
	};

	/**
	 * Exception class
	 */
	class Exception: public std::exception {
	private:

		/**
		 * message about the exception
		 */
		string message;

		/**
		 * exception type
		 */
		int type;

	public:

		/**
		 * constructor
		 *
		 * @param message message about the exception
		 * @param type of the exception
		 */
		EXL Exception(string message, int type);

		/**
		 * constructor, initializes type with 0
		 *
		 * @param message message about the exception
		 */
		EXL Exception(string message);

		/**
		 * constructor, initializes message with ""
		 * and type with 0
		 */
		EXL Exception();

		/**
		 * copy constructor
		 */
		EXL Exception(const Exception &e);

		/**
		 * assignment operator
		 */
		EXL Exception &operator = (const Exception &e);

		/**
		 * setter
		 */
		EXL void setMessage(string message);

		/**
		 * getter
		 */
		EXL string getMessage() const;

		/**
		 * setter
		 */
		EXL void setType(int type);

		/**
		 * getter
		 */
		EXL int getType() const;
	};

}

#endif