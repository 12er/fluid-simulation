#ifndef _SX_MATH_H_
#define _SX_MATH_H_

/**
 * SX math classes
 * (c) 2012 by Tristan Bauer
 */

#include <export/Export.h>

namespace sx {

	/**
	 * constant pi, one half of the circumference
	 * of the unit circle
	 */
	const float Pi =  3.1415926535f;

	/**
	 * 2 * pi, the full circumference
	 * of the unit circle
	 */
	const float Tau = 6.2831853072f;

	/**
	 * the eulerian number
	 */
	const float Exp = 2.7182818285f;

	class Matrix;

	/**
	 * A Vector in four-dimensional homogenous space
	 */
	class Vector {
	public:

		/**
		 * components of vector,
		 * has always four components x,y,z,w
		 */
		float *elements;

		/**
		 * default constructor, constructs
		 * a zero vector with w=1
		 */
		EXPA Vector();

		/**
		 * constructor, initializes x,y,z with v
		 * and w=1
		 */
		EXPA Vector(float v);

		/**
		 * constructor, initializes the vector
		 * with (x,y,0,1)
		 */
		EXPA Vector(float x,float y);

		/**
		 * constructor, initializes the vector
		 * with (x,y,z,1)
		 */
		EXPA Vector(float x,float y,float z);

		/**
		 * constructor, initializes the vector
		 * with (x,y,z,w)
		 */
		EXPA Vector(float x,float y,float z,float w);

		/**
		 * Constructor, copies array v into elements.
		 * v must have at least length 4.
		 */
		EXPA Vector(const float *v);

		/**
		 * copy constructor
		 */
		EXPA Vector(const Vector &vector);

		/**
		 * assignment operator
		 */
		EXPA Vector &operator = (const Vector &v);

		/**
		 * deconstructor
		 */
		EXPA virtual ~Vector();

		/**
		 * Returns a reference to the index. component.
		 * If index is out of bounds, an exception is thrown.
		 *
		 * @param index number of the component
		 * @param return reference to the index. component
		 * @param exception throws an exception Exception of type EX_BOUNDS, if index is equal to 4 or above
		 */
		EXPA float &operator [] (unsigned int index);

		/**
		 * Returns a reference to the index. component.
		 * If index is out of bounds, an exception is thrown.
		 *
		 * @param index number of the component
		 * @param return reference to the index. component
		 * @param exception throws an exception Exception of type EX_BOUNDS, if index is equal to 4 or above
		 */
		EXPA float operator [] (unsigned int index) const;

		/**
		 * copies vector v, and returns a reference to this
		 */
		EXPA Vector &operator << (const Vector &v);

		/**
		 * Copies array v, and returns a reference to this.
		 * The length of array v must be at least 4.
		 * Returns a reference to this.
		 */
		EXPA Vector &operator << (const float *v);

		/**
		 * copies itself to vector v, returns a reference to this
		 */
		EXPA const Vector &operator >> (Vector &v) const;

		/**
		 * Copies itself to array v. The length of array v
		 * must be at least 4. Returns a reference to this.
		 */
		EXPA const Vector &operator >> (float *v) const;

		/**
		 * adds vector v to this vector, leaves w untouched,
		 * and returns a reference to this
		 */
		EXPA Vector &add(const Vector &v);

		/**
		 * add vector (x,x,x,1) to this vector, leaves w untouched,
		 * and returns a reference to this
		 */
		EXPA Vector &add(float x);

		/**
		 * returns the sum of v1 and v2 with w=1
		 */
		EXPA friend Vector operator +(const Vector &v1, const Vector &v2);

		/**
		 * returns the sum of v and (x,x,x,1) with w=1
		 */
		EXPA friend Vector operator +(const Vector &v, float x);

		/**
		 * returns the sum of v and (x,x,x,1) with w=1
		 */
		EXPA friend Vector operator +(float x, const Vector &v);

		/**
		 * stores the crossproduct of this and v in this, leaves w untouched,
		 * and returns a reference to this
		 */
		EXPA Vector &crossmult(const Vector &v);

		/**
		 * returns the crossproduct of v1 and v2 with w=1
		 */
		EXPA friend Vector operator %(const Vector &v1, const Vector &v2);

		/**
		 * multiplies this with s, leaves w untouched, and
		 * returns a reference to this
		 */
		EXPA Vector &scalarmult(float s);

		/**
		 * returns the scalarproduct of v and x and copies w of v into
		 * the returned vector
		 */
		EXPA friend Vector operator *(const Vector &v, float x);

		/**
		 * returns the scalarproduct of v and x and copies w of v into
		 * the returned vector
		 */
		EXPA friend Vector operator *(float x, const Vector &v);

		/**
		 * returns the scalarproduct of v with -1 and copies w of v into
		 * the returned vector
		 */
		EXPA friend Vector operator -- (const Vector &v);

		/**
		 * returns the inner product of the first three components
		 * of this and v
		 */
		EXPA float innerprod(const Vector &v) const;

		/**
		 * returns the inner product of the first three components
		 * of v1 and v2
		 */
		EXPA friend float operator *(const Vector &v1, const Vector &v2);

		/**
		 * stores m * this[0] in this
		 */
		EXPA Vector &leftmult(const Matrix &m);

		/**
		 * stores this[0] * m in this
		 */
		EXPA Vector &rightmult(const Matrix &m);

		/**
		 * returns m * v
		 */
		EXPA friend Vector operator *(const Matrix &m, const Vector &v);

		/**
		 * returns v * m
		 */
		EXPA friend Vector operator *(const Vector &v, const Matrix &m);

		/**
		 * Normalizes this leaving w untouched, and taking only the components
		 * x, y, z into consideration, and returns a reference to this.
		 */
		EXPA Vector &normalize();

		/**
		 * returns the distance of the points specified by the first three components
		 * of this and v
		 */
		EXPA float distance(const Vector &v) const;

		/**
		 * returns the length of (x,y,z)
		 */
		EXPA float length() const;

		/**
		 * assigns (x,y,z) with a random value out of [0,1]^3, sets w=1,
		 * and returns a reference to this
		 */
		EXPA Vector &random();

		/**
		 * Homogenizes the vector by dividing x, y, z, w by w,
		 * and returns a reference to this. If w is equal to zero,
		 * the method does not modify this.
		 */
		EXPA Vector &homogenize();

		/**
		 * returns true iff all components of this and v are equal
		 */
		EXPA bool equals(const Vector &v) const;

		/**
		 * returns true iff the difference of each component of this and v
		 * is not larger than epsilon
		 */
		EXPA bool equals(const Vector &v, float epsilon) const;

	};

	/**
	 * A 4 x 4 matrix
	 */
	class Matrix {
	public:

		/**
		 * Elements of the matrix, has always 16 of them.
		 * The elements are ordered in column-major order: Let s0, s1, s2, s3
		 * be the column vectors of the matrix, then the elements of the matrix
		 * in the array elements are in the same order as the elements of
		 * the tuple (s0,s1,s2,s3).
		 */
		float *elements;

		/**
		 * default constructor, initializes the matrix
		 * with the identity matrix
		 */
		EXPA Matrix();

		/**
		 * constructor, initializes the matrix with
		 * all values set to m
		 */
		EXPA Matrix(float m);

		/**
		 * Constructor. Initializes the matrix with the unit matrix,
		 * overwritten in its above left by the 2x2 matrix
		 * m00 m01
		 * m10 m11
		 * .
		 */
		EXPA Matrix(
			float m00, float m01,
			float m10, float m11);

		/**
		 * Constructor. Initializes the matrix with the unit matrix,
		 * overwritten in its above left by the 3x3 matrix
		 * m00 m01 m02
		 * m10 m11 m12
		 * m20 m21 m22
		 * .
		 */
		EXPA Matrix(
			float m00, float m01, float m02,
			float m10, float m11, float m12,
			float m20, float m21, float m22);

		/**
		 * Constructor. Initializes the matrix with the matrix
		 * m00 m01 m02 m03
		 * m10 m11 m12 m13
		 * m20 m21 m22 m23
		 * m30 m31 m32 m33
		 * .
		 */
		EXPA Matrix(
			float m00, float m01, float m02, float m03,
			float m10, float m11, float m12, float m13,
			float m20, float m21, float m22, float m23,
			float m30, float m31, float m32, float m33);

		/**
		 * constructor, initializes the matrix with the
		 * columns m0, m1, m2, m3
		 */
		EXPA Matrix(const Vector &m0, const Vector &m1, const Vector &m2, const Vector &m3);

		/**
		 * Constructor, initializes the matrix with the 
		 * entries of array m. m is required to be at least
		 * of length 16, and the matrix elements in m should
		 * be given in column-major order.
		 */
		EXPA Matrix(const float *m);

		/**
		 * copy constructor
		 */
		EXPA Matrix(const Matrix &m);

		/**
		 * assignment operator
		 */
		EXPA Matrix &operator = (const Matrix &m);

		/**
		 * deconstructor
		 */
		EXPA virtual ~Matrix();

		/**
		 * Returns a reference to the index. component.
		 * If index is out of bounds, an exception is thrown.
		 *
		 * @param index number of the component
		 * @param return reference to the index. component
		 * @param exception throws an exception Exception of type EX_BOUNDS, if index is equal to 16 or above
		 */
		EXPA float &operator [] (unsigned int index);

		/**
		 * Returns a reference to the index. component.
		 * If index is out of bounds, an exception is thrown.
		 *
		 * @param index number of the component
		 * @param return reference to the index. component
		 * @param exception throws an exception Exception of type EX_BOUNDS, if index is equal to 16 or above
		 */
		EXPA float operator [] (unsigned int index) const;

		/**
		 * copies matrix m, and returns a reference to this
		 */
		EXPA Matrix &operator << (const Matrix &m);

		/**
		 * Copies array m, and returns a reference to this.
		 * The length of array m must be at least 16.
		 * The elements of the matrix in m should be given
		 * in column-major order.
		 * Returns a reference to this.
		 */
		EXPA Matrix &operator << (const float *m);

		/**
		 * copies itself to matrix m, returns a reference to this
		 */
		EXPA const Matrix &operator >> (Matrix &m) const;

		/**
		 * Copies itself to array m. The content of this is
		 * copied into m in column-major order. The length of array v
		 * must be at least 16. Returns a reference to this.
		 */
		EXPA const Matrix &operator >> (float *m) const;

		/**
		 * adds matrix m to this vector,
		 * and returns a reference to this
		 */
		EXPA Matrix &add(const Matrix &m);

		/**
		 * adds a matrix with all components equal to m
		 * to this, and returns a reference to this
		 */
		EXPA Matrix &add(float m);

		/**
		 * returns the sum of m1 and m2
		 */
		EXPA friend Matrix operator + (const Matrix &m1, const Matrix &m2);

		/**
		 * returns the sum of m and a matrix with all components
		 * equal to x
		 */
		EXPA friend Matrix operator + (const Matrix &m, float x);

		/**
		 * returns the sum of m and a matrix with all components
		 * equal to x
		 */
		EXPA friend Matrix operator + (float x, const Matrix &m);

		/**
		 * stores m * this[0] in this, and returns a reference
		 * to this
		 */
		EXPA Matrix &leftmult(const Matrix &m);

		/**
		 * stores this[0] * m in this, and returns a reference
		 * to this
		 */
		EXPA Matrix &rightmult(const Matrix &m);

		/**
		 * returns the scalar product of this and s
		 */
		EXPA Matrix &scalarmult(float s);

		/**
		 * returns the product m1 * m2
		 */
		EXPA friend Matrix operator * (const Matrix &m1, const Matrix &m2);

		/**
		 * returns the scalar product m * s
		 */
		EXPA friend Matrix operator * (const Matrix &m, float s);

		/**
		 * returns the scalar product m * s
		 */
		EXPA friend Matrix operator * (float s, const Matrix &m);

		/**
		 * returns the product -1 * m
		 */
		EXPA friend Matrix operator - (const Matrix &m);

		/**
		 * transposes this, and returns a reference to this
		 */
		EXPA Matrix &transpose();

		/**
		 * returns a transposed copy of m
		 */
		EXPA friend Matrix operator ! (const Matrix &m);

		/**
		 * sets this to the identity matrix, and
		 * returns a reference to this
		 */
		EXPA Matrix &identity();

		/**
		 * Replaces itself with its inverse matrix.
		 * Leaves this untouched, if the matrix is not regular.
		 * Returns a reference to this.
		 */
		EXPA Matrix &inverse();

		/**
		 * Returns the power of a matrix. If matrix m is regular, and power
		 * is negative, the |power|. power of the inverse of m
		 * is computed. If m is singular, instead the |power|. power of m
		 * is computed.
		 */
		EXPA friend Matrix operator ^ (const Matrix &m, int power);

		/**
		 * returns the determinant of the matrix
		 */
		EXPA float determinant() const;

		/**
		 * returns the cofactor a_(row,column) of the matrix
		 */
		EXPA float cofactor(unsigned int row, unsigned int column) const;

		/**
		 * assigns this with a random matrix in [0,1]^4x4, and
		 * returns a reference to this
		 */
		EXPA Matrix &random();

		/**
		 * assigns this with the rotation matrix representing
		 * a rotation around m by angle in radians counterclockwise,
		 * and returns a reference to this
		 */
		EXPA Matrix &rotate(const Vector &m, float angle);

		/**
		 * assigns this with the translation matrix representing
		 * a translation by vector v, and returns a
		 * reference to this
		 */
		EXPA Matrix &translate(const Vector &v);

		/**
		 * Assigns this with the scaling matrix representing
		 * a scaling by v[0] along the x-axis, by v[1]
		 * along the y-axis and by v[2] along the z-axis.
		 * Returns a reference to this.
		 */
		EXPA Matrix &scale(const Vector &v);

		/**
		 * Assigns this with the shear matrix representing
		 * a shear with the x-y plane being a plane of fixed points,
		 * and v being the shearvector of the shear mapping.
		 * Vectors parallel to the shear vector are transformed
		 * such that they are parallel to the z-axis.
		 * The third component of v must me different from zero,
		 * otherwise the method does not change this.
		 * Returns a reference to this.
		 */
		EXPA Matrix &shear(const Vector &v);

		/**
		 * assigns this with its 3x3 submatrix in the
		 * left above corner, and returns a reference to this
		 */
		EXPA Matrix &submatrix();

		/**
		 * Assigns this with its normal matrix.
		 * A normal matrix of a 4x4 matrix m is defined as the transposed
		 * inverted matrix of the 3x3 submatrix in the left above corner of m.
		 * Let's the invertion of the 3x3 submatrix being undone, if
		 * it's not a regular matrix.
		 * Returns a reference to this.
		 */
		EXPA Matrix &normalMatrix();

		/**
		 * Assigns this with a transformation into the viewspace having
		 * its origin at position, its z-axis pointing in negative view direction,
		 * and its y-axis pointing in up direction. Vectors view and up are required
		 * to span a plane, otherwise the method will have no effect.
		 * Returns a reference to this.
		 */
		EXPA Matrix &viewMatrix(const Vector &position, const Vector &view, const Vector &up);

		/**
		 * Assigns this with a transformation performing a perspective transform from
		 * viewspace into projection space. The viewing direcion in view space is the negative
		 * z-axis, which remains to be the viewing direction in projection space after the transform.
		 * Parameter angle specifies the viewing angle of the viewing frustrum in the y-z-plane in radians.
		 * width and height are the width and height of the projection plane. Along the viewing direction
		 * the viewing frustrum starts at -znear, and ends at -zfar.
		 * In projection space the viewing frustrum is the unit cube, with -1 on the front, and 1 on the back.
		 * Hence along the z-axis -znear in viewing space equals -1 in projection space, and
		 * -zfar in viewing space equals 1 in projection space.
		 * The specified viewing frustrum must be nonempty, otherwise this method does not change this matrix.
		 */
		EXPA Matrix &perspectiveMatrix(float angle, float width, float height, float znear, float zfar);
		
		/**
		 * Assigns this with a transformation performing an orthogonal transform from viewspace
		 * into projection space. The view frustrum is bounded by left, right along the x-axis,
		 * bottom, top along the y-axis,
		 * and along the z-axis it starts znear behind the x-y-plane, and ends zfar behind the x-y-plane.
		 * In projection space the viewing frustrum is the unit cube, with -1 on the front, and 1 on the back.
		 * Hence along the z-axis -znear in viewing space equals -1 in projection space, and
		 * -zfar in viewing space equals 1 in projection space.
		 * The specified viewing frustrum must be nonempty, otherwise this method does not change this matrix.
		 */
		EXPA Matrix &orthographicPerspeciveMatrix(float left, float right, float bottom, float top, float znear, float zfar);

		/**
		 * returns true iff all components of this and m are equal
		 */
		EXPA bool equals(const Matrix &m) const;

		/**
		 * returns true iff the difference of each component of this and m
		 * is not larger than epsilon
		 */
		EXPA bool equals(const Matrix &m, float epsilon) const;
	};

}

#endif