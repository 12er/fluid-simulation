#ifndef _EXPORT_EXPORT_H_
#define _EXPORT_EXPORT_H_

/**
 * Export Header
 * (c) 2012 by Tristan Bauer
 */
#include <platform/PlatformSpecific.h>
#include <export/ProjectSpecific.h>

	/**
	 * Default:
	 * No Log4SX export
	 */
	#define EXL
	/**
	 * Default:
	 * No SXParse export
	 */
	#define EXPA
	/**
	 * Default:
	 * No ShadeX Engine export
	 */
	#define EX
	/**
	 * Default:
	 * No ShadeX Widget export
	 */
	#define EXW

	#ifdef _PROJECT_LOG4SX_
		/**
		* Log4SX is a framework, and
		* its interfaces are exported with the
		* aid of EXL
		*/
		#ifdef _PLATFORM_WINDOWS_
			/**
			 * In Windows the interfaces are
			 * exported to a dll
			 */
			#undef EXL
			#define EXL __declspec(dllexport)
		#endif
	#endif

	#ifdef _PROJECT_SXPARSER_
		/**
		* SXParse is a framework, and
		* its interfaces are exported with the
		* aid of EXL
		*/
		#ifdef _PLATFORM_WINDOWS_
			/**
			 * In Windows the interfaces are
			 * exported to a dll
			 */
			#undef EXPA
			#define EXPA __declspec(dllexport)
		#endif
		/**
		 * For running the test cases,
		 * logging must be activated,
		 * for the working build it should
		 * be deactivated by commenting the
		 * following define
		 */
//		#define _IS_LOGGING_
	#endif

	#ifdef _PROJECT_SHADEXENGINE_
		/**
		* The ShadeX Engine is a framework, and
		* its interfaces are exported with the
		* aid of EX
		*/
		#ifdef _PLATFORM_WINDOWS_
			/**
			 * In Windows the interfaces are
			 * exported to a dll
			 */
			#undef EX
			#define EX __declspec(dllexport)
		#endif
		/**
		 * For running the test cases,
		 * logging must be activated,
		 * for the working build it should
		 * be deactivated by commenting the
		 * following define
		 */
//		#define _IS_LOGGING_
	#endif

	#ifdef _PROJECT_SHADEXENGINEWIDGET_
		/**
		* The ShadeXEngineWidget is a QWidget
		* making the use of the ShadeXEngine in
		* a Qt Application possible. Its interfaces
		* are exported with the aid of EXW
		*/
		#ifdef _PLATFORM_WINDOWS_
			/**
			 * In Windows the interfaces are
			 * exported to a dll
			 */
			#undef EXW
			#define EXW __declspec(dllexport)
		#endif
		/**
		 * For running the test cases,
		 * logging must be activated,
		 * for the working build it should
		 * be deactivated by commenting the
		 * following define
		 */
//		#define _IS_LOGGING_
	#endif

	#ifdef _PROJECT_SHADEXENGINEEDITOR_
		/**
		 * The ShadeX Engine Editor does not
		 * require any exported interfaces
		 */
	#endif

	#ifdef _PROJECT_SHADEXENGINEDEMO_
		/**
		 * The ShadeX Engine Demo does not
		 * require any exported interfaces
		 */
	#endif

	#ifdef _PROJECT_TESTSX_
		/**
		 * TestSX does not
		 * require any exported interfaces
		 */
//		#define _IS_LOGGING_
	#endif

#endif