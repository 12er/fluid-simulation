#version 400 core
in vec2 vtexcoords;
out vec4 target;
uniform sampler2D source;

void main() {
	vec4 u = texture2D(source,vtexcoords);
	u.rg = (u.rg - vec2(0.5)) * 2.0;
	target = u;
}
