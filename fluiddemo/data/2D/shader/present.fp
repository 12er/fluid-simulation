#version 400 core
in vec2 vtexcoords;
out vec4 frag1;
uniform sampler2D data;
uniform sampler2D pressure;
uniform sampler2D velocity;

void main() {
	if(vtexcoords.x < 1.0 && vtexcoords.y < 1.0) {
		frag1 = texture2D(data,vtexcoords);
	} else if(vtexcoords.y < 1.0) {
		vec3 p = texture2D(pressure,vtexcoords).rgb;
		p = p/100.0;
		if(p.r > 1.0) {
			p.g = p.r/10.0;
			p.r = 1.0 - p.g;
		}
		frag1 = vec4(p,1);
	} else if(vtexcoords.x < 1.0) {
		frag1 = texture2D(velocity,vtexcoords);
	} else {
		frag1 = vec4(0,1,0,1);
	}
}
