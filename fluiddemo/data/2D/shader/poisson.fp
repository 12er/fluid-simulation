#version 400 core
in vec2 vtexcoords;
out vec4 newpressure;
uniform sampler2D pressure;
uniform sampler2D velocity;
uniform vec4 delta;

void main() {
	vec2 dxp = vec2(delta.x,0);
	vec2 dxm = vec2(-delta.x,0);
	vec2 dyp = vec2(0,delta.y);
	vec2 dym = vec2(0,-delta.y);
	
	//jacobi iteration calculating pressure times timedelta divided by density by solving the poisson equation for fluids
	//the negative gradient of this scalarfield is equal to the force caused by the pressure acting on the particle continuum over a time timedelta
	//the force caused by the pressure removes any divergence in the velocity field

	//calculate divergence, which is one side of the poisson equation for fluids
	float divergence = (texture2D(velocity,vtexcoords + dxp).r - texture2D(velocity,vtexcoords + dxm).r + texture2D(velocity,vtexcoords + dyp).g - texture2D(velocity,vtexcoords + dym).g);
	
	//lookup of the old pressure values
	float oldpressure = texture2D(pressure,vtexcoords+dxp).r + texture2D(pressure,vtexcoords+dxm).r + texture2D(pressure,vtexcoords+dyp).r + texture2D(pressure,vtexcoords+dym).r;
	
	//performing one jacobi iteration
	newpressure = vec4((oldpressure-divergence)*0.25,0,0,1);
}
