#version 400 core
in vec2 vtexcoords;
out vec4 newvelocity;
uniform sampler2D velocity;
uniform vec4 inputposition;
uniform vec4 inputspeed;
uniform float deltatime;

void main() {
	newvelocity = texture2D(velocity,vtexcoords);
	
	if(inputposition.x - 0.05 < vtexcoords.x && inputposition.x + 0.05 > vtexcoords.x && inputposition.y - 0.05 < vtexcoords.y && inputposition.y + 0.05 > vtexcoords.y) {
		newvelocity = inputspeed * 6000.0;
	}
}
