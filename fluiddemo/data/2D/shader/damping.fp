#version 400 core
in vec2 vtexcoords;
out vec4 newquantity;
uniform sampler2D quantity;
uniform float deltatime;

void main() {
	vec3 q = texture2D(quantity,vtexcoords).rgb;
	q = q * (1.0 - deltatime * 0.2);
	newquantity = vec4(q,1);
}
