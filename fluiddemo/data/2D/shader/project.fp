#version 400 core
in vec2 vtexcoords;
out vec4 newvelocity;
uniform sampler2D velocity;
uniform sampler2D pressure;
uniform vec4 delta;

void main() {
	vec2 dxp = vec2(delta.x,0);
	vec2 dxm = vec2(-delta.x,0);
	vec2 dyp = vec2(0,delta.y);
	vec2 dym = vec2(0,-delta.y);

	//calculates pressure of the pressure times deltatime divided by density
	vec2 gradient = vec2(texture2D(pressure,vtexcoords + dxp).r - texture2D(pressure,vtexcoords + dxm).r , texture2D(pressure,vtexcoords + dyp).r - texture2D(pressure,vtexcoords + dym).r);
	
	vec4 u = texture2D(velocity,vtexcoords);
	
	//apply force caused by pressure over time deltatime to the speed
	u.rg = u.rg - gradient;
	newvelocity = u;
}
