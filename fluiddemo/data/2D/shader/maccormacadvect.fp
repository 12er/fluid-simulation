#version 400 core
in vec2 vtexcoords;
out vec4 newquantity;
uniform sampler2D quantity;
uniform sampler2D velocity;
uniform float kerneldelta;
uniform float deltatime;
uniform vec4 delta;
uniform mat4 fluid2texture;

void main() {
	//The speed of a particle specifies the change of the particle in fluid volume width per second.
	//A fluid volume in texture space has 2 * d.x width and 2 * d.y height. This way the partial derivations
	//of a function f in texture coordinates is can be computed with 1.0 in its denominator:
	//(f(x + d.x,y) - f(x - d.x,y))/tex2worldspace.x(2 * d.x) = (f(x + d.x,y) - f(x - d.x,y))/1
	//(f(x,y + d.y) - f(x,y + d.y))/tex2worldspace.y(2 * d.y) = (f(x,y + d.y) - f(x,y - d.y))/1
	
	vec4 current = texture2D(quantity,vtexcoords);

	vec2 d = vec2(delta);
	vec2 predictorv = texture2D(velocity,vtexcoords).rg;
	vec2 predictorp = vtexcoords - deltatime * vec2(predictorv.x * d.x * 2.0,predictorv.y * d.y * 2.0);
	vec4 predictor = texture2D(quantity,predictorp);
	
	vec2 correctorv = texture2D(velocity,predictorp).rg;
	vec2 correctorp = predictorp + deltatime * vec2(correctorv.x * d.x * 2.0 , correctorv.y * d.y * 2.0);
	vec4 corrector = texture2D(quantity,correctorp);	

	newquantity = predictor + 0.5 * (current - corrector);
}
