#version 400 core
in vec2 vtexcoords;
out vec4 target;
uniform sampler2D source;

void main() {
	target = texture2D(source,vtexcoords);
}
