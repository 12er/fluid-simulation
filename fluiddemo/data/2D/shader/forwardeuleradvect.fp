#version 400 core
in vec2 vtexcoords;
out vec4 newquantity;
uniform sampler2D quantity;
uniform sampler2D velocity;
uniform float kerneldelta;
uniform float deltatime;
uniform vec4 delta;
uniform mat4 fluid2texture;

void main() {
	vec2 d = vec2(delta);
	vec2 u = texture2D(velocity,vtexcoords).rg;
	
	//The speed of a particle specifies the change of the particle in fluid volume width per second.
	//A fluid volume in texture space has 2 * d.x width and 2 * d.y height. This way the partial derivations
	//of a function f in texture coordinates is can be computed with 1.0 in its denominator:
	//(f(x + d.x,y) - f(x - d.x,y))/tex2worldspace.x(2 * d.x) = (f(x + d.x,y) - f(x - d.x,y))/1
	//(f(x,y + d.y) - f(x,y + d.y))/tex2worldspace.y(2 * d.y) = (f(x,y + d.y) - f(x,y - d.y))/1
	
	//lookup particle at its previous position
	vec2 oldp = vtexcoords - deltatime * vec2(u.x * d.x * 2.0,u.y * d.y * 2.0);
	
	//calcutate an average value in the area of the previous particle
	//and use the value for this pixel
	vec2 dxp = vec2(kerneldelta,0);
	vec2 dxm = vec2(-kerneldelta,0);
	vec2 dyp = vec2(0,kerneldelta);
	vec2 dym = vec2(0,-kerneldelta);
	vec4 q = texture2D(quantity,oldp);
	vec4 qxp = texture2D(quantity,oldp + dxp);
	vec4 qxm = texture2D(quantity,oldp + dxm);
	vec4 qyp = texture2D(quantity,oldp + dyp);
	vec4 qym = texture2D(quantity,oldp + dym);
	newquantity = (q + qxp + qxm + qyp + qym)/5.0;
}
