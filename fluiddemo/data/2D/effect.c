/**
 * defines the structure of the rendering pipeline
 */
(effects) {

	/**
	 * transform fluid space to texture space
	 */
	(matrix) {
		id = "fluid2texture";
		(value) {
			"0.569105" "0"
			"0" "1"
		}
	}

	/**
	 * distance of cells in grid times one half
	 */
	(vector) {
		id = "delta";
		(value) {
			"0.00227642" "0.004"
		}
	}

	/**
	 * length of timeslice
	 */
	(float) {
		id = "deltatime";
		value = 0.001;
	}

	/**
	 * speed and position of input
	 */
	(vector) {
		id = "input.position";
	}

	(vector) {
		id = "input.speed";
	}

	/**
	 * postprocess quad
	 */
	(mesh) {
		id = "postprocess";
		path = "../data/mesh/target.ply";
	}

	(object) {
		id = "postprocess.obj";
		mesh = "postprocess";
		(passes) {
			"advect.preparevelocity"
			"advect.forwardeuler.velocity"
			"advect.maccormack.velocity"
			"advect.velocity.copyback"
			"advect.forwardeuler.pressure"
			"advect.maccormack.pressure"
			"advect.pressure.copyback"
			"advect.forwardeuler.data"
			"advect.maccormack.data"
			"advect.data.copyback"
			"poisson"
			"poisson.copyback"
			"project"
			"project.copyback"
			"mainscene"
			"input"
			"input.copyback"
			"reset.data"
			"reset.velocity"
			"reset.pressure"
			"damping.velocity"
			"damping.velocity.copyback"
			"damping.pressure"
			"damping.pressure.copyback"
		}
	}

	/**
	 * invoces all passes responsible
	 * for the main engine
	 */
	(effect) {
		id = "maineffect.forwardeuler";

		(passes) {
			"advect.forwardeuler.pressure"
			"advect.pressure.copyback"
			"advect.forwardeuler.data"
			"advect.data.copyback"
			"advect.preparevelocity"
			"advect.forwardeuler.velocity"
			"advect.velocity.copyback"
			"poisson"
			"poisson.copyback"
			"project"
			"project.copyback"
			"mainscene"
		}
	}

	(effect) {
		id = "maineffect.maccormack";

		(passes) {
			"advect.maccormack.pressure"
			"advect.pressure.copyback"
			"advect.maccormack.data"
			"advect.data.copyback"
			"advect.preparevelocity"
			"advect.maccormack.velocity"
			"advect.velocity.copyback"
			"poisson"
			"poisson.copyback"
			"project"
			"project.copyback"
			"mainscene"
		}
	}

	//rendering scene to display

	(pass) {
		id = "mainscene";
		shader = "mainscene.shader";
		(textures) {
			data = "flow.data";
			pressure = "flow.pressure";
			velocity = "flow.velocity";
		}
		(output) {
			rendertarget = "mainscene.display";
		}
	}

	(rendertarget) {
		id = "mainscene.display";
		width = 984;
		height = 560;
		renderToDisplay = "true";
	}

	/**
	 * advect fluid
	 */

	(float) {
		id = "advect.velocity.kerneldelta";
		value = 0.00005;
	}

	(float) {
		id = "advect.kerneldelta";
		value = 0;
	}

	//advect velocity

	(pass) {
		id = "advect.preparevelocity";
		shader = "copy.shader";
		(textures) {
			source = "flow.velocity";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.input";
		}
	}

	(pass) {
		id = "advect.forwardeuler.velocity";
		shader = "advect.forwardeuler.shader";
		(textures) {
			quantity = "flow.input";
			velocity = "flow.velocity";
		}
		(floats) {
			kerneldelta = "advect.velocity.kerneldelta";
			deltatime = "deltatime";
		}
		(vectors) {
			delta = "delta";
		}
		(matrices) {
			fluid2texture = "fluid2texture";
		}
		(output) {
			rendertarget = "flow.target";
			newquantity = "flow.output";
		}
	}

	(pass) {
		id = "advect.maccormack.velocity";
		shader = "advect.maccormack.shader";
		(textures) {
			quantity = "flow.input";
			velocity = "flow.velocity";
		}
		(floats) {
			kerneldelta = "advect.velocity.kerneldelta";
			deltatime = "deltatime";
		}
		(vectors) {
			delta = "delta";
		}
		(matrices) {
			fluid2texture = "fluid2texture";
		}
		(output) {
			rendertarget = "flow.target";
			newquantity = "flow.output";
		}
	}

	(pass) {
		id = "advect.velocity.copyback";
		shader = "copy.shader";
		(textures) {
			source = "flow.output";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.velocity";
		}
	}

	//advect pressure

	(pass) {
		id = "advect.forwardeuler.pressure";
		shader = "advect.forwardeuler.shader";
		(textures) {
			quantity = "flow.pressure";
			velocity = "flow.velocity";
		}
		(floats) {
			kerneldelta = "advect.kerneldelta";
			deltatime = "deltatime";
		}
		(vectors) {
			delta = "delta";
		}
		(matrices) {
			fluid2texture = "fluid2texture";
		}
		(output) {
			rendertarget = "flow.target";
			newquantity = "flow.output";
		}
	}

	(pass) {
		id = "advect.maccormack.pressure";
		shader = "advect.maccormack.shader";
		(textures) {
			quantity = "flow.pressure";
			velocity = "flow.velocity";
		}
		(floats) {
			kerneldelta = "advect.kerneldelta";
			deltatime = "deltatime";
		}
		(vectors) {
			delta = "delta";
		}
		(matrices) {
			fluid2texture = "fluid2texture";
		}
		(output) {
			rendertarget = "flow.target";
			newquantity = "flow.output";
		}
	}

	(pass) {
		id = "advect.pressure.copyback";
		shader = "copy.shader";
		(textures) {
			source = "flow.output";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.pressure";
		}
	}

	//advect data

	(pass) {
		id = "advect.forwardeuler.data";
		shader = "advect.forwardeuler.shader";
		(textures) {
			quantity = "flow.data";
			velocity = "flow.velocity";
		}
		(floats) {
			kerneldelta = "advect.kerneldelta";
			deltatime = "deltatime";
		}
		(vectors) {
			delta = "delta";
		}
		(matrices) {
			fluid2texture = "fluid2texture";
		}
		(output) {
			rendertarget = "flow.target";
			newquantity = "flow.output";
		}
	}

	(pass) {
		id = "advect.maccormack.data";
		shader = "advect.maccormack.shader";
		(textures) {
			quantity = "flow.data";
			velocity = "flow.velocity";
		}
		(floats) {
			kerneldelta = "advect.kerneldelta";
			deltatime = "deltatime";
		}
		(vectors) {
			delta = "delta";
		}
		(matrices) {
			fluid2texture = "fluid2texture";
		}
		(output) {
			rendertarget = "flow.target";
			newquantity = "flow.output";
		}
	}

	(pass) {
		id = "advect.data.copyback";
		shader = "copy.shader";
		(textures) {
			source = "flow.output";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.data";
		}
	}

	/**
	 * solve poisson equation for fluids
	 */

	(pass) {
		id = "poisson";
		shader = "poisson.shader";
		(textures) {
			pressure = "flow.pressure";
			velocity = "flow.velocity";
		}
		(vectors) {
			delta = "delta";
		}
		(output) {
			rendertarget = "flow.target";
			newpressure = "flow.output";
		}
	}

	(pass) {
		id = "poisson.copyback";
		shader = "copy.shader";
		(textures) {
			source = "flow.output";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.pressure";
		}
	}

	/**
	 * let pressure influence velocity
	 */

	(pass) {
		id = "project";
		shader = "project.shader";
		(textures) {
			pressure = "flow.pressure";
			velocity = "flow.velocity";
		}
		(vectors) {
			delta = "delta";
		}
		(output) {
			rendertarget = "flow.target";
			newvelocity = "flow.output";
		}
	}

	(pass) {
		id = "project.copyback";
		shader = "copy.shader";
		(textures) {
			source = "flow.output";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.velocity";
		}
	}

	/**
	 * take user input
	 */

	//move particles by changing speed

	(effect) {
		id = "input.effect";
		(passes) {
			"input"
			"input.copyback"
		}
	}

	(pass) {
		id = "input";
		shader = "input.shader";
		(textures) {
			velocity = "flow.velocity";
		}
		(floats) {
			deltatime = "deltatime";
		}
		(vectors) {
			inputposition = "input.position";
			inputspeed = "input.speed";
		}
		(output) {
			rendertarget = "flow.target";
			newvelocity = "flow.output";
		}
	}

	(pass) {
		id = "input.copyback";
		shader = "copy.shader";
		(textures) {
			source = "flow.output";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.velocity";
		}
	}

	//reset data to initial state

	(effect) {
		id = "reset.data.effect";
		(passes) {
			"reset.data"
		}
	}

	(pass) {
		id = "reset.data";
		shader = "copy.shader";
		(textures) {
			source = "start.data";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.data";
		}
	}

	//reset velocity to initial state

	(effect) {
		id = "reset.velocity.effect";
		(passes) {
			"reset.velocity"
		}
	}

	(pass) {
		id = "reset.velocity";
		shader = "initvelocity.shader";
		(textures) {
			source = "start.velocity";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.velocity";
		}
	}

	//reset pressure to initial state

	(effect) {
		id = "reset.pressure.effect";
		(passes) {
			"reset.pressure"
		}
	}

	(pass) {
		id = "reset.pressure";
		shader = "copy.shader";
		(textures) {
			source = "start.pressure";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.pressure";
		}
	}

	//reset everything

	(effect) {
		id = "reset.effect";
		(passes) {
			"reset.data"
			"reset.velocity"
			"reset.pressure"
		}
	}

	//damp velocity

	(effect) {
		id = "damping.velocity.effect";
		(passes) {
			"damping.velocity"
			"damping.velocity.copyback"
		}
	}

	(pass) {
		id = "damping.velocity";
		shader = "damping.shader";
		(textures) {
			quantity = "flow.velocity";
		}
		(floats) {
			deltatime = "deltatime";
		}
		(output) {
			rendertarget = "flow.target";
			newquantity = "flow.output";
		}
	}

	(pass) {
		id = "damping.velocity.copyback";
		shader = "copy.shader";
		(textures) {
			source = "flow.output";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.velocity";
		}
	}

	//damp pressure

	(effect) {
		id = "damping.pressure.effect";
		(passes) {
			"damping.pressure"
			"damping.pressure.copyback"
		}
	}

	(pass) {
		id = "damping.pressure";
		shader = "damping.shader";
		(textures) {
			quantity = "flow.pressure";
		}
		(floats) {
			deltatime = "deltatime";
		}
		(output) {
			rendertarget = "flow.target";
			newquantity = "flow.output";
		}
	}

	(pass) {
		id = "damping.pressure.copyback";
		shader = "copy.shader";
		(textures) {
			source = "flow.output";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.pressure";
		}
	}

	//damp velocity and pressure

	(effect) {
		id = "damping.effect";
		(passes) {
			"damping.velocity"
			"damping.velocity.copyback"
			"damping.pressure"
			"damping.pressure.copyback"
		}
	}

}
