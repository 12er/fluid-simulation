/**
 * resource allocation
 */
(resources) {

	//generic rendertarget for data, pressure, velocity and output

	(rendertarget) {
		id = "flow.target";
		width = 984;
		height = 560;
	}

	//textures of the flow simulation at the start

	(texture) {
		id = "start.data";
		path = "../data/texture/ShadeXEngine.jpg";
	}

	(texture) {
		id = "start.pressure";
		path = "../data/texture/pressure.jpg";
	}

	(texture) {
		id = "start.velocity";
		path = "../data/texture/velocity.jpg";
	}

	//textures of the flow simulation after start

	(texture) {
		id = "flow.data";
		width = 984;
		height = 560;
		format = "float";
	}

	(texture) {
		id = "flow.pressure";
		width = 984;
		height = 560;
		format = "float";
	}

	(texture) {
		id = "flow.velocity";
		width = 984;
		height = 560;
		format = "float";
	}

	(texture) {
		id = "flow.input";
		width = 984;
		height = 560;
		format = "float";
	}

	(texture) {
		id = "flow.output";
		width = 984;
		height = 560;
		format = "float";
	}

	(effect) {
		id = "copystart.effect";
		(passes) {
			"copystart.data"
			"copystart.pressure"
			"copystart.velocity"
		}
	}

	(pass) {
		id = "copystart.data";
		shader = "copy.shader";
		(textures) {
			source = "start.data";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.data";
		}
	}

	(pass) {
		id = "copystart.pressure";
		shader = "copy.shader";
		(textures) {
			source = "start.pressure";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.pressure";
		}
	}

	(pass) {
		id = "copystart.velocity";
		shader = "initvelocity.shader";
		(textures) {
			source = "start.velocity";
		}
		(output) {
			rendertarget = "flow.target";
			target = "flow.velocity";
		}
	}

	(object) {
		id = "copystart.object";
		mesh = "postprocess";
		(passes) {
			"copystart.data"
			"copystart.pressure"
			"copystart.velocity"
		}
	}

	//mainscene shader
	
	(shader) {
		id = "mainscene.shader";
		vertex = "../data/2D/shader/present.vp";
		fragment = "../data/2D/shader/present.fp";
	}

	//copy shader

	(shader) {
		id = "copy.shader";
		vertex = "../data/2D/shader/copy.vp";
		fragment = "../data/2D/shader/copy.fp";
	}

	//shader for initialization of velocity

	(shader) {
		id = "initvelocity.shader";
		vertex = "../data/2D/shader/initvelocity.vp";
		fragment = "../data/2D/shader/initvelocity.fp";
	}

	//advection shader

	(shader) {
		id = "advect.forwardeuler.shader";
		vertex = "../data/2D/shader/forwardeuleradvect.vp";
		fragment = "../data/2D/shader/forwardeuleradvect.fp";
	}

	(shader) {
		id = "advect.maccormack.shader";
		vertex = "../data/2D/shader/maccormacadvect.vp";
		fragment = "../data/2D/shader/maccormacadvect.fp";
	}

	//shader solving the poisson equation for fluids

	(shader) {
		id = "poisson.shader";
		vertex = "../data/2D/shader/poisson.vp";
		fragment = "../data/2D/shader/poisson.fp";
	}

	//apply pressure on velocity leaving only its
	//divergence free component

	(shader) {
		id = "project.shader";
		vertex = "../data/2D/shader/project.vp";
		fragment = "../data/2D/shader/project.fp";
	}

	//take user input

	(shader) {
		id = "input.shader";
		vertex = "../data/2D/shader/input.vp";
		fragment = "../data/2D/shader/input.fp";
	}

	//damp quantity like speed

	(shader) {
		id = "damping.shader";
		vertex = "../data/2D/shader/damping.vp";
		fragment = "../data/2D/shader/damping.fp";
	}

}
