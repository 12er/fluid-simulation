/**
 * 3D fluid simulation
 */
(effects) {

	(mesh) {
		id = "process.mesh";
		path = "../data/mesh/target.ply";
	}

	(object) {
		id = "process.object";
		mesh = "process.mesh";
		(passes) {
			"copysource.pass"
			"resetpv.pass"
			"combinedjacobiadvection.pass"
			"advect.velocitypressure.pass"
			"advect.data.pass"
			"maccormackadvect.data.pass"
			"maccormackadvect.velocitypressure.pass"
			"data.copyback.pass"
			"jacobi.pass"
			"project.pass"
			"grabbing.pass"
			"grabbing.copyback.pass"
			"mainscene.pass"
		}
	}

	(volume) {
		id = "source.volume";
		(paths) {
			"../data/3D/f1.png"
			"../data/3D/f2.png"
			"../data/3D/f3.png"
			"../data/3D/f4.png"
			"../data/3D/f5.png"
			"../data/3D/f6.png"
			"../data/3D/f7.png"
			"../data/3D/f8.png"
			"../data/3D/f9.png"
			"../data/3D/f10.png"
			"../data/3D/f11.png"
		}
	}

	(volume) {
		id = "data.volume";
		width = 160;
		height = 160;
		depth = 160;

		format = "byte";
	}

	(volume) {
		id = "data2.volume";
		width = 160;
		height = 160;
		depth = 160;

		format = "byte";
	}

	(volume) {
		id = "velocitypressure.volume";
		width = 160;
		height = 160;
		depth = 160;
		format = "float16";
	}

	(volume) {
		id = "velocitypressure2.volume";
		width = 160;
		height = 160;
		depth = 160;
		format = "float16";
	}

	(volume) {
		id = "velocitypressure3.volume";
		width = 160;
		height = 160;
		depth = 160;
		format = "float16";
	}

	//init data

	(rendertarget) {
		id = "volume.target";
		width = 160;
		height = 160;
	}

	(pass) {
		id = "copysource.pass";
		shader = "copyprogram.shader";
		(volumes) {
			source = "source.volume";
		}
		(volumeoutput) {
			rendertarget = "volume.target";
			destination = "data.volume";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
		}
	}

	(shader) {
		id = "resetpv.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;
			
			void main() {
				gl_Position = vertices;
				vtc = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			out vec4 destination;
			in vec2 vtc;

			uniform float depthcoordinate;
			
			void main() {
				destination = vec4(0,0,0,0);
			}
		"}

	}

	(pass) {
		id = "resetpv.pass";
		shader = "resetpv.shader";
		(volumeoutput) {
			rendertarget = "volume.target";
			destination = "velocitypressure.volume";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
		}
	}

	(effect) {
		id = "copystart.effect";
		(passes) {
			"copysource.pass"
			"resetpv.pass"
		}
	}

	(effect) {
		id = "resetdata.effect";
		(passes) {
			"copysource.pass"
		}
	}

	(effect) {
		id = "resetfluid.effect";
		(passes) {
			"resetpv.pass"
		}
	}

	//advect

	(shader) {
		id = "combinedjacobiadvect.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vertices;
				vtc = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 velpress2;

			uniform float dx;
			uniform float deltatime;
			uniform float depthcoordinate;
			uniform sampler3D velpress;

			void main() {
				vec3 vp = vec3(vtc,depthcoordinate);
				vec3 vel = texture(velpress,vp).xyz * 2.0 * dx * deltatime;
				vec3 oldp = vp - vel;

				vec3 delx = vec3(dx,0,0);
				vec3 dely = vec3(0,dx,0);
				vec3 delz = vec3(0,0,dx);

				vec4 velp = texture(velpress,oldp);
				vec4 vel_x = texture(velpress,oldp + delx);
				vec4 vel_xm = texture(velpress,oldp - delx);
				vec4 vel_y = texture(velpress,oldp + dely);
				vec4 vel_ym = texture(velpress,oldp - dely);
				vec4 vel_z = texture(velpress,oldp + delz);
				vec4 vel_zm = texture(velpress,oldp - delz);

				float divergence = 
					vel_x.x - vel_xm.x +
					vel_y.y - vel_ym.y +
					vel_z.z - vel_zm.z
					;

				float oldpressure = 
					vel_x.a + vel_xm.a +
					vel_y.a + vel_ym.a +
					vel_z.a + vel_zm.a
					;

				velpress2 = vec4(velp.xyz , (oldpressure - divergence) * 0.1666666666);
			}
		
		"}
	}

	(shader) {
		id = "advect.velocitypressure.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vertices;
				vtc = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 velpres2;

			uniform float dx;
			uniform float deltatime;
			uniform float depthcoordinate;
			uniform sampler3D velpres;

			void main() {
				vec3 vp = vec3(vtc,depthcoordinate);
				vec3 vel = texture(velpres,vp).xyz * 2.0 * dx * deltatime;
				vec3 oldp = vp - vel;
				velpres2 = texture(velpres,oldp);
			}
		
		"}

	}
	
	(shader) {
		id = "advect.data.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vertices;
				vtc = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 data2;

			uniform float dx;
			uniform float deltatime;
			uniform float depthcoordinate;
			uniform sampler3D velpress;
			uniform sampler3D data;

			void main() {
				vec3 vp = vec3(vtc,depthcoordinate);
				vec3 vel = texture(velpress,vp).xyz * 2.0 * dx * deltatime;
				vec3 oldp = vp - vel;
				data2 = texture(data,oldp);
			}
		
		"}

	}

	(shader) {
		id = "maccormackadvect.velocitypressure.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vertices;
				vtc = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 velpress2;

			uniform float dx;
			uniform float deltatime;
			uniform float depthcoordinate;
			uniform sampler3D velpress;

			void main() {
				vec3 vp = vec3(vtc,depthcoordinate);
				vec4 current = texture(velpress,vp);
				vec3 vel = current.xyz * 2.0 * dx * deltatime;
				vec3 predictorp = vp - vel;
				vec4 predictor = texture(velpress,predictorp);
				vec3 velpredictor = predictor.xyz * 2.0 * dx * deltatime;
				vec3 correctorp = predictorp + velpredictor;
				vec4 corrector = texture(velpress,correctorp);
				velpress2 = predictor + 0.5 * (current - corrector);
			}
		
		"}

	}

	(shader) {
		id = "maccormackadvect.data.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vertices;
				vtc = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 data2;

			uniform float dx;
			uniform float deltatime;
			uniform float depthcoordinate;
			uniform sampler3D velpress;
			uniform sampler3D data;

			void main() {
				vec3 vp = vec3(vtc,depthcoordinate);
				vec3 vel = texture(velpress,vp).xyz * 2.0 * dx * deltatime;
				vec3 predictorp = vp - vel;
				vec3 velpredictor = texture(velpress,predictorp).xyz * 2.0 * dx * deltatime;
				vec3 correctorp = predictorp + velpredictor;

				vec4 current = texture(data,vp);
				vec4 predictor = texture(data,predictorp);
				vec4 corrector = texture(data,correctorp);
				data2 = predictor + 0.5 * (current - corrector);
			}
		
		"}

	}

	(pass) {
		id = "advect.data.pass";
		shader = "advect.data.shader";
		(floats) {
			dx = "dx.float";
			deltatime = "deltatime.float";
		}
		(volumes) {
			data = "data.volume";
			velpress = "velocitypressure.volume";
		}
		(volumeoutput) {
			rendertarget = "volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			data2 = "data2.volume";
		}
	}

	(pass) {
		id = "data.copyback.pass";
		shader = "copyprogram.shader";
		(volumes) {
			source = "data2.volume";
		}
		(volumeoutput) {
			rendertarget = "volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			destination = "data.volume";
		}
	}

	(pass) {
		id = "advect.velocitypressure.pass";
		shader = "advect.velocitypressure.shader";
		(floats) {
			dx = "dx.float";
			deltatime = "deltatime.float";
		}
		(volumes) {
			velpres = "velocitypressure.volume";
		}
		(volumeoutput) {
			rendertarget = "volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			velpres2 = "velocitypressure2.volume";
		}
	}

	(pass) {
		id = "combinedjacobiadvection.pass";
		shader = "combinedjacobiadvect.shader";
		(floats) {
			dx = "dx.float";
			deltatime = "deltatime.float";
		}
		(volumes) {
			velpress = "velocitypressure.volume";
		}
		(volumeoutput) {
			rendertarget = "volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			velpress2 = "velocitypressure3.volume";
		}
	}

	(pass) {
		id = "maccormackadvect.data.pass";
		shader = "maccormackadvect.data.shader";
		(floats) {
			dx = "dx.float";
			deltatime = "deltatime.float";
		}
		(volumes) {
			data = "data.volume";
			velpress = "velocitypressure.volume";
		}
		(volumeoutput) {
			rendertarget = "volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			data2 = "data2.volume";
		}
	}

	(pass) {
		id = "maccormackadvect.velocitypressure.pass";
		shader = "maccormackadvect.velocitypressure.shader";
		(floats) {
			dx = "dx.float";
			deltatime = "deltatime.float";
		}
		(volumes) {
			velpress = "velocitypressure.volume";
		}
		(volumeoutput) {
			rendertarget = "volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			velpress2 = "velocitypressure2.volume";
		}
	}

	//jacobiiteration

	(shader) {
		id = "jacobi.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vertices;
				vtc = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 velpress2;

			uniform float dx;
			uniform float depthcoordinate;
			uniform sampler3D velpress;

			void main() {
				vec3 vp = vec3(vtc,depthcoordinate);
				vec3 vel = texture(velpress,vp).rgb;
				vec3 delx = vec3(dx,0,0);
				vec3 dely = vec3(0,dx,0);
				vec3 delz = vec3(0,0,dx);

				float divergence =
					texture(velpress,vp + delx).x - texture(velpress,vp - delx).x +
					texture(velpress,vp + dely).y - texture(velpress,vp - dely).y +
					texture(velpress,vp + delz).z - texture(velpress,vp - delz).z
					;

				float oldpressure =
					texture(velpress,vp + delx).a + texture(velpress,vp - delx).a +
					texture(velpress,vp + dely).a + texture(velpress,vp - dely).a +
					texture(velpress,vp + delz).a + texture(velpress,vp - delz).a
					;

				velpress2 = vec4(vel,(oldpressure - divergence)*0.16666666);
			}
		
		"}

	}

	(pass) {
		id = "jacobi.pass";
		shader = "jacobi.shader";
		(floats) {
			dx = "dx.float";
		}
		(volumes) {
			velpress = "velocitypressure2.volume";
		}
		(volumeoutput) {
			rendertarget = "volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			velpress2 = "velocitypressure3.volume";
		}
	}

	//project divergence free part

	(shader) {
		id = "project.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vertices;
				vtc = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 velpress2;

			uniform float dx;
			uniform float depthcoordinate;
			uniform sampler3D velpress;

			void main() {
				vec3 vp = vec3(vtc,depthcoordinate);
				vec4 velp = texture(velpress,vp);
				vec3 delx = vec3(dx,0,0);
				vec3 dely = vec3(0,dx,0);
				vec3 delz = vec3(0,0,dx);

				vec3 gradient = vec3(
					texture(velpress,vp + delx).a - texture(velpress,vp - delx).a ,
					texture(velpress,vp + dely).a - texture(velpress,vp - dely).a ,
					texture(velpress,vp + delz).a - texture(velpress,vp - delz).a
					);

				velpress2 = vec4(velp.xyz - gradient,velp.a);
			}
		
		"}

	}

	(pass) {
		id = "project.pass";
		shader = "project.shader";
		(floats) {
			dx = "dx.float";
		}
		(volumes) {
			velpress = "velocitypressure3.volume";
		}
		(volumeoutput) {
			rendertarget = "volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			velpress2 = "velocitypressure.volume";
		}
	}

	//simulation effect

	//half of the width
	//of a fluid element
	(float) {
		id = "dx.float";
		value = 0.01;
	}

	(float) {
		id = "deltatime.float";
		value = 0.3;
	}

	(effect) {
		id = "combinedjacobiadvection.effect";
		(passes) {
			"advect.data.pass"
			"data.copyback.pass"
			"combinedjacobiadvection.pass"
			"project.pass"
		}
	}

	(effect) {
		id = "forwardeuler.effect";
		(passes) {
			"advect.data.pass"
			"data.copyback.pass"
			"advect.velocitypressure.pass"
			"jacobi.pass"
			"project.pass"
		}
	}

	(effect) {
		id = "maccormack.effect";
		(passes) {
			"maccormackadvect.data.pass"
			"data.copyback.pass"
			"maccormackadvect.velocitypressure.pass"
			"jacobi.pass"
			"project.pass"
		}
	}

	//grabbing effect

	(vector) {
		id = "grabbing.grabPosition";
	}

	(vector) {
		id = "grabbing.grabVelocity";
	}

	(shader) {
		id = "grabbing.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vertices;
				vtc = texcoords;
			}
		
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 velpress2;

			uniform vec4 grabPosition;
			uniform vec4 grabVelocity;
			uniform float depthcoordinate;
			uniform sampler3D velpress;

			void main() {
				vec3 vp = vec3(vtc,depthcoordinate);
				velpress2 = texture(velpress,vp);

				if(vp.x - grabPosition.x > -0.05 && vp.x - grabPosition.x < 0.05 &&
					vp.y - grabPosition.y > -0.05 && vp.y - grabPosition.y < 0.05 &&
					vp.z - grabPosition.z > -0.05 && vp.z - grabPosition.z < 0.05) {
					velpress2 = grabVelocity;
				}
			}
			
		"}
	}

	(pass) {
		id = "grabbing.pass";
		shader = "grabbing.shader";
		(vectors) {
			grabPosition = "grabbing.grabPosition";
			grabVelocity = "grabbing.grabVelocity";
		}
		(volumes) {
			velpress = "velocitypressure.volume";
		}
		(volumeoutput) {
			rendertarget = "volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			velpress2 = "velocitypressure2.volume";
		}
	}

	(pass) {
		id = "grabbing.copyback.pass";
		shader = "copyprogram.shader";
		(volumes) {
			source = "velocitypressure2.volume";
		}
		(volumeoutput) {
			rendertarget = "volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			destination = "velocitypressure.volume";
		}
	}

	(effect) {
		id = "grabbing.effect";
		(passes) {
			"grabbing.pass"
			"grabbing.copyback.pass"
		}
	}

	//copyprogram

	(shader) {
		id = "copyprogram.shader";
		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vertices;
				vtc = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 destination;
			
			uniform float depthcoordinate;
			uniform sampler3D source;

			void main() {
				destination = texture(source,vec3(vtc,depthcoordinate));
			}
		"}

	}

	//present volume

	(vector) {
		id = "mainscene.camera.eye";
	}

	(matrix) {
		id = "mainscene.camera.view";
	}

	(matrix) {
		id = "mainscene.camera.viewinv";
	}

	(matrix) {
		id = "mainscene.camera.projection";
	}

	(matrix) {
		id = "mainscene.camera.projectioninv";
	}

	(shader) {
		id = "mainscene.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			//points on the projectionsurface
			//in viewspacecoordinates
			out vec4 projpoints;

			void main() {
				gl_Position = vertices;
				vtc = texcoords;
				projpoints = vec4(vertices.xy,-1,1);
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			in vec4 projpoints;
			out vec4 frag1;

			//cameraposition
			uniform vec4 eye;
			//transformation from viewspace
			//to worldspace
			uniform mat4 view2world;
			//transformation from projectionspace
			//to viewspace
			uniform mat4 proj2view;
			uniform sampler3D vol;

			void main() {
				mat4 view2coord = mat4(vec4(0.5,0,0,0),vec4(0,0.5,0,0),vec4(0,0,0.5,0),vec4(0.5,0.5,0.5,1));
				vec3 e = eye.xyz;
				vec4 pi = view2world * proj2view * projpoints;
				vec3 p = pi.xyz/pi.w;
				vec3 ray = p - e;
				ray = normalize(ray) * 0.03;

				vec4 s = vec4(0);
				for(int i=0 ; i<100 ; i++) {
					vec3 cp = (view2coord * vec4(clamp(p,-1,1),1)).xyz;
					if(cp.x != 0 && cp.x != 1 && cp.y != 0 && cp.y != 1 && cp.z != 0 && cp.z != 1) {
						s = s + vec4(texture(vol,cp).rgb,1);
					}
					
					p = p + ray;
				}
				s = s * 0.01 * 5.0;

				frag1 = s;
			}
		"}

	}

	(rendertarget) {
		id = "mainscene.display";
		width = 1024;
		height = 800;
		renderToDisplay = "true";
	}

	(pass) {
		id = "mainscene.pass";
		shader = "mainscene.shader";
		(vectors) {
			eye = "mainscene.camera.eye";
		}
		(matrices) {
			proj2view = "mainscene.camera.projectioninv";
			view2world = "mainscene.camera.viewinv";
		}
		(volumes) {
			vol = "data.volume";
		}
		(output) {
			rendertarget = "mainscene.display";
		}
	}

	(effect) {
		id = "mainscene.effect";
		(passes) {
			"mainscene.pass"
		}
	}

}
