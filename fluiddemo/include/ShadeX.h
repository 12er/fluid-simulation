#ifndef _SHADEX_H_
#define _SHADEX_H_

#include <export/Export.h>
#include <sx/SX.h>

namespace sx {

	class SX {
	public:
		static ShadeX shadeX;
	};

}

#endif