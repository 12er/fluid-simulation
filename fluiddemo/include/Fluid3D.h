#ifndef _FLUID3D_H_
#define _FLUID3D_H_

#include <sx/SX.h>
#include <QObject>
#include <QWidget>
#include <QComboBox>
#include <QPushButton>
#include <QString>
#include <QLabel>
#include <QLineEdit>
using namespace sx;

namespace fluid {

	
	enum FluidAlgorithm {
		FLUID_COMBINEDADVECT,
		FLUID_FORWARDEULER,
		FLUID_MACCORMACK
	};

	class FluidSolver;

	class FluidWidget: public QWidget {

		Q_OBJECT

	private:

		static const QString algorithmCombinedAdvect;
		static const QString algorithmForwardEuler;
		static const QString algorithmMacCormack;

		static const QString runSimulation;
		static const QString stopSimulation;

		static const QString statusRunning;
		static const QString statusPause;

		static const QString format_float16;
		static const QString format_float32;

		QComboBox *algorithmSettings;
		QPushButton *runSimulationButton;

		QLineEdit *widthField;
		QLineEdit *heightField;
		QLineEdit *depthField;
		QComboBox *formatCombo;

		QLineEdit *dxField;

		FluidSolver *solver;

		FluidWidget(const FluidWidget &);
		FluidWidget &operator = (const FluidWidget &);

	private slots:

		void resendSetAlgorithm();

		void resendSetRunSimulation();

		void resendResetSimulation();

		void resendResetData();

		void resendResetFluid();

		void resendSetGridDimensions();

		void resendSetDX();

	signals:

		void setAlgorithm(FluidAlgorithm algorithm);

		void setRunSimulation(bool run);

		void resetSimulation();

		void resetData();

		void resetFluid();

		void setGridDimensions(int width, int height, int depth, int format);

		void setDX(float dx);

	public slots:

		void showRunSimulation();

		void showPauseSimulation();

	public:

		FluidWidget();

	};

	class FluidSolver: public QObject, public SXRenderListener {

		Q_OBJECT

	private:

		int width;

		int height;

		bool holdMouse;

		int holdMouseX;

		int holdMouseY;

		int lastMouseX;

		int lastMouseY;

		bool grabbing;

		Vector oldGrabPos;

		Vector eye;

		Vector view;

		Vector up;

		FluidAlgorithm algorithm;

		bool pauseSimulation;

		bool pressedSpace;

		FluidSolver(const FluidSolver &);

		FluidSolver &operator = (const FluidSolver &);

	public slots:

		void setAlgorithm(FluidAlgorithm algorithm);

		void setRunSimulation(bool run);

		void resetSimulation();

		void resetData();

		void resetFluid();

		void setGridDimensions(int width, int height, int depth, int format);

		void setDX(float dx);

	signals:

		void isRunningSimulation();

		void isPausingSimulation();

	public:

		FluidSolver();

		~FluidSolver();

		void create(SXRenderArea &area);

		void reshape(SXRenderArea &area);

		void render (SXRenderArea &area);

		void stop(SXRenderArea &area);

		int getGridWidth();
		
		int getGridHeight();

		int getGridDepth();

		float getDX();

	};

}

#endif