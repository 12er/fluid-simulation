#ifndef _FLUID2D_H_
#define _FLUID2D_H_

#include <sx/SX.h>
using namespace sx;

namespace fluid {

	enum FluidAlgorithm {
		FLUID_FORWARDEULER,
		FLUID_MACCORMACK
	};

	class FluidSolver: public SXRenderListener {
	private:

		int width;

		int height;

		int lastMouseX;

		int lastMouseY;

		FluidAlgorithm algorithm;

		FluidSolver(const FluidSolver &);

		FluidSolver &operator = (const FluidSolver &);

	public:

		FluidSolver();

		~FluidSolver();

		void create(SXRenderArea &area);

		void reshape(SXRenderArea &area);

		void render (SXRenderArea &area);

		void stop(SXRenderArea &area);

	};

}

#endif